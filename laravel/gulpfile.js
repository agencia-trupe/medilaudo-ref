var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;
//elixir.config.autoprefixerOptions.browsers = ['IE 9', 'Safari 8'];

var paths = {
	src : {
		less : 'resources/assets/less/',
		js : 'resources/assets/js/',
		vendor : './resources/assets/vendor/',
	},
	output : {
		less : '../public/assets/css/',
		js : '../public/assets/js/',
		fonts : '../public/assets/css/fonts/',
		img : '../public/assets/images/',
		vendor: '../public/assets/vendor/'
	},
};

elixir(function(mix) {
    mix
    	// --------------------------------//
    	// Cópia de arquivos originais da
    	// pasta Vendor para o acesso público
    	// --------------------------------//
    	.copy( paths.src.vendor + 'modernizr/modernizr.js', paths.output.vendor + 'modernizr.js', './')
    	.copy( paths.src.vendor + 'jquery/dist/jquery.min.js', paths.output.vendor + 'jquery.js', './')
    	.copy( paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js', paths.output.vendor + 'bootstrap.js', './')
        .copy( paths.src.vendor + 'bootstrap/dist/fonts/', paths.output.fonts)
        .copy( paths.src.vendor + 'ckeditor/', paths.output.vendor + 'ckeditor/')

        .copy( paths.src.vendor + 'fancybox/source/fancybox_loading.gif', paths.output.less + 'site/')
        .copy( paths.src.vendor + 'fancybox/source/fancybox_sprite.png', paths.output.less + 'site/')
        .copy( paths.src.vendor + 'fancybox/source/fancybox_overlay.png', paths.output.less + 'site/')
		.copy( paths.src.vendor + 'jquery-ui/themes/base/images', paths.output.less + 'painel/images/')

        // Executar somente 1 vez \/
        //.copy( paths.src.vendor + 'ckeditor/config.js', paths.src.js + 'painel/ckeditor_config.js')


    	// --------------------------------//
    	// Estilos e Scripts do Site
    	// --------------------------------//

		.less( paths.src.less + 'site/main.less', paths.src.less + 'site' )
		.less( paths.src.less + 'site/internas.less', paths.src.less + 'site' )

    	.styles([
		    paths.src.vendor + 'css-reset/reset.min.css',
            paths.src.vendor + 'fancybox/source/jquery.fancybox.css',
			paths.src.vendor + 'tooltipster/dist/css/tooltipster.bundle.css',
			paths.src.vendor + 'tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css',
			paths.src.less + 'site/main.css',
			paths.src.less + 'site/internas.css',
    	], paths.output.less + 'site/main.css', './')

    	.scripts([
            paths.src.vendor + 'fancybox/source/jquery.fancybox.js',
			paths.src.vendor + 'tooltipster/dist/js/tooltipster.bundle.js',
            paths.src.js + 'site/site.js'
        ], paths.output.js + 'site.js', './')


    	// --------------------------------//
    	// Estilos e Scripts do Painel
    	// --------------------------------//

    	.less([
            paths.src.less + 'painel/painel.less'
        ], paths.src.less + 'build_painel', './')

        // ESTILOS
    	.styles([
    	    paths.src.vendor + 'css-reset/reset.min.css',
			paths.src.vendor + 'jquery-ui/themes/base/jquery-ui.css',
			paths.src.vendor + 'bootstrap/dist/css/bootstrap.min.css',
			paths.src.less   + 'build_painel/painel.css',
    	], paths.output.less + 'painel/painel.css', './')

        // SCRIPTS
        .scripts( paths.src.js + 'painel/ckeditor_config.js', paths.output.js + 'ckeditor_config.js', './')

        .scripts([
            paths.src.vendor + 'jquery-ui/jquery-ui.min.js',
            paths.src.vendor + 'bootbox/bootbox.js',
            paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js',
            paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',
		    paths.src.js		 + 'painel/painel-upload-imagens.js',
            paths.src.js     + 'painel/painel.js'
        ], paths.output.js  + 'painel.js', './');

});
