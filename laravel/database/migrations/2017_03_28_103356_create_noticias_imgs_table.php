<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias_imagens', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('noticias_id')->unsigned();
            $table->foreign('noticias_id')->references('id')->on('noticias')->onDelete('cascade');

            $table->string('imagem');
            $table->integer('ordem')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias_imagens');
    }
}
