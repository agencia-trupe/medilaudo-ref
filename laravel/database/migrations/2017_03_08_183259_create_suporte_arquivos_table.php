<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuporteArquivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suporte_arquivos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('categorias_id')->unsigned();
            $table->foreign('categorias_id')->references('id')->on('suporte_categorias')->onDelete('cascade');

            $table->string('titulo');
            $table->text('descritivo');
            $table->string('arquivo');
            $table->integer('ordem')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suporte_arquivos');
    }
}
