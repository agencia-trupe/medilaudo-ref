<?php

use Illuminate\Database\Seeder;

class EstruturaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('estrutura')->delete();

      DB::table('estrutura')->insert([
        [
          'subtitulo' => 'Subtítulo Empresa',
          'texto' => 'Texto'
        ]
      ]);
    }
}
