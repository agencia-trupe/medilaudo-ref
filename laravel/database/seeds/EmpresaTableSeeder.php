<?php

use Illuminate\Database\Seeder;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('empresa')->delete();

      DB::table('empresa')->insert([
        [
          'subtitulo' => 'Subtítulo Empresa',
          'texto_esquerda' => 'Texto Esquerda',
          'texto_direita' => 'Texto Direita',
        ]
      ]);
    }
}
