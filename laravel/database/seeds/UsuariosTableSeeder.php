<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('admin')->delete();

      DB::table('admin')->insert([
        [
          'nome' => 'trupe',
          'login' => 'trupe',
          'email' => 'contato@trupe.net',
          'password' => Hash::make('senhatrupe')
        ]
      ]);
    }
}
