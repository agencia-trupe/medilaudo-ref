<?php

use Illuminate\Database\Seeder;

class ChamadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('chamadas')->delete();

      DB::table('chamadas')->insert([
        [
          'texto' => 'AGILIDADE NO DIAGNÓSTICO COM LAUDOS EM ATÉ 2 HORAS',
          'link' => '',
          'ordem' => 0
        ],
        [
          'texto' => 'TODAS AS ESPECIALIDADES À DISPOSIÇÃO REUNIDAS NUM SÓ LUGAR',
          'link' => '',
          'ordem' => 1
        ],
        [
          'texto' => 'REDUÇÃO DE CUSTOS E MAIOR OFERTA DE SERVIÇOS AOS PACIENTES',
          'link' => '',
          'ordem' => 2
        ],
        [
          'texto' => 'PADRONIZAÇÃO DOS SERVIÇOS COM ATENDIMENTO ONLINE POR 24 HORAS 7 DIAS POR SEMANA',
          'link' => '',
          'ordem' => 3
        ]
      ]);
    }
}
