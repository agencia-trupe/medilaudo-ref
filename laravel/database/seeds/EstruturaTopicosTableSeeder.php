<?php

use Illuminate\Database\Seeder;

class EstruturaTopicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('estrutura_topicos')->delete();

      DB::table('estrutura_topicos')->insert([
        [
          'titulo' => 'Especialistas',
          'texto' => 'Texto especialistas',
          'imagem_1' => '',
          'imagem_2' => ''
        ],
        [
          'titulo' => 'Gestão',
          'texto' => 'Texto gestão',
          'imagem_1' => '',
          'imagem_2' => ''
        ],
        [
          'titulo' => 'Informática',
          'texto' => 'Texto informática',
          'imagem_1' => '',
          'imagem_2' => ''
        ],
      ]);
    }
}
