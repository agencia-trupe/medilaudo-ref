<?php

use Illuminate\Database\Seeder;

class LaudosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('laudos')->delete();

      DB::table('laudos')->insert([
        [
          'titulo' => 'NEURORRADIOLOGIA',
          'slug' => 'neurorradiologia',
          'ordem' => 0
        ],
        [
          'titulo' => 'MEDICINA INTERNA',
          'slug' => 'medicina-interna',
          'ordem' => 1
        ],
        [
          'titulo' => 'MUSCULOESQUELÉTICO',
          'slug' => 'musculoesqueletico',
          'ordem' => 2
        ],
        [
          'titulo' => 'RADIOLOGIA GERAL',
          'slug' => 'radiologia-geral',
          'ordem' => 3
        ],
        [
          'titulo' => 'RAIOS X',
          'slug' => 'raios-x',
          'ordem' => 4
        ],
        [
          'titulo' => 'DENSITOMETRIA ÓSSEA',
          'slug' => 'densitometria-ossea',
          'ordem' => 5
        ],
        [
          'titulo' => 'MAMOGRAFIA / RESSONÂNCIA DE MAMA',
          'slug' => 'mamografia-ressonancia-de-mama',
          'ordem' => 6
        ],
        [
          'titulo' => 'PET-CT',
          'slug' => 'pet-ct',
          'ordem' => 7
        ]
      ]);
    }
}
