<?php

use Illuminate\Database\Seeder;

class LaudosVantagensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('laudos_vantagens')->delete();

      DB::table('laudos_vantagens')->insert([
        [
          'titulo' => 'PARA OS MÉDICOS',
          'texto' => '<p>Texto Para os Médicos</p>',
          'ordem' => 0
        ],
        [
          'titulo' => 'PARA OS HOSPITAIS E CLÍNICAS',
          'texto' => '<p>Texto para os Hosputais e Clínicas</p>',
          'ordem' => 1
        ],
        [
          'titulo' => 'PARA A SOCIEDADE',
          'texto' => '<p>Texto para a sociedade</p>',
          'ordem' => 2
        ],
        [
          'titulo' => 'PARA OS PACIENTES',
          'texto' => '<p>Texto para os pacientes</p>',
          'ordem' => 3
        ]
      ]);
    }
}
