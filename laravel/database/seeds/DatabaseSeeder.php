<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(ChamadasTableSeeder::class);
      $this->call(ContatoTableSeeder::class);
      $this->call(EmpresaTableSeeder::class);
      $this->call(EstruturaTableSeeder::class);
      $this->call(EstruturaTopicosTableSeeder::class);
      $this->call(LaudosTableSeeder::class);
      $this->call(LaudosVantagensTableSeeder::class);
      $this->call(UsuariosTableSeeder::class);
      $this->call(VideoHomeTableSeeder::class);
    }
}
