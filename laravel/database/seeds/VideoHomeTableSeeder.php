<?php

use Illuminate\Database\Seeder;

class VideoHomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('video_home')->delete();

      DB::table('video_home')->insert([
        [
          'link_video' => ''
        ]
      ]);
    }
}
