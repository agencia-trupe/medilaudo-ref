<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('contato')->delete();

      DB::table('contato')->insert([
        [
          'telefone' => '11 5083-6277',
          'endereco' => 'Rua Borges Lagoa 1083 &bull; 13&ord; andar &bull; Vila Clementino',
          'google_maps' => '',
          'linkedin' => '',
          'facebook' => ''
        ]
      ]);
    }
}
