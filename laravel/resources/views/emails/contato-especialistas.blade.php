<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body style="font-family: 'Roboto', 'Oxygen', 'Ubuntu', 'Helvetica Neue', sans-serif; color:#000;font-size:16px;">
  <h3>Mensagem de contato via formulário do site</h3>
  <p style="line-height: 140%;">
    <b>Nome:</b> {{ $nome }}<br>
    <b>E-mail:</b> {{ $email }}<br>
    <b>Telefone:</b> {{ $telefone }}<br>
    <b>Especialidade:</b> {{ $especialidade }}<br>
    <b>Título especialista no CBR:</b> {{ $titulo_cbr }}<br>
    <b>Mensagem:</b> {{ $mensagem }}<br>
  </p>
</body>
</html>
