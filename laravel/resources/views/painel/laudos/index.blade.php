@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Laudos</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.laudos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Laudo</a>

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela='laudos'>

          <thead>
          	<tr>
              <th>Ordenar</th>
              <th style="width: 5%;">Destaque</th>
          		<th>Título</th>
              <th><span class="glyphicon glyphicon-user"></span></th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                <td style="width: 5%;">
                  @if($registro->destaque)
                    <button type="button" class="btn btn-success toggle-destaque toggle-laudo-destaque" data-id="{{$registro->id}}">
                      <span class="glyphicon glyphicon-ok"></span>
                      <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                    </button>
                  @else
                    <button type="button" class="btn btn-danger toggle-destaque toggle-laudo-destaque" data-id="{{$registro->id}}">
                      <span class="glyphicon glyphicon-remove"></span>
                      <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                    </button>
                  @endif
                </td>
                <td>
                  {{$registro->titulo}}
                </td>
                <td><a href="{{ URL::route('painel.especialistas.index', ['laudos_id' => $registro->id]) }}" class="btn btn-sm btn-default">especialistas</a></td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.laudos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.laudos.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
