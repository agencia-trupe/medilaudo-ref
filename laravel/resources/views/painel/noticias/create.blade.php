@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Notícia</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.noticias.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

  			    <div class="form-group">
  			      <label for="inputChamada">Chamada</label>
  			      <input type="text" name="chamada" class="form-control" id="inputChamada" value="{{old('chamada')}}">
  			    </div>

            <div class="well" id="accordion">

              <label><input type="radio" name="tipo_destaque" value="destaque_video" @if(old('tipo_destaque') == 'destaque_video') checked @endif > Destaque em Vídeo</label>
              <div id="destaque_video" @if(old('tipo_destaque') == 'destaque_video') style="display:block;" @else style="display:none;" @endif >
                <div class="form-group">
                  <input type="text" name="destaque_link_video" placeholder="Link do vídeo (Youtube)" class="form-control" id="inputLink" value="{{old('destaque_link_video')}}">
                </div>
              </div>

              <hr>

              <label><input type="radio" name="tipo_destaque" value="destaque_imagem" @if(old('tipo_destaque') == 'destaque_imagem') checked @endif > Destaque em Imagem</label>
              <div id="destaque_img" @if(old('tipo_destaque') == 'destaque_imagem') style="display:block;" @else style="display:none;" @endif >
                <div class="form-group">
                  @if(old('destaque_imagem'))
                    Imagem atual<br>
                    <img src="assets/images/noticias/{{old('destaque_imagem')}}"><br>
                  @endif
                  <input type="file" class="form-control" id="inputImagem" name="destaque_imagem">
                </div>
              </div>

              <hr>

              <label><input type="radio" name="tipo_destaque" value="sem_destaque" @if(old('tipo_destaque') == 'sem_destaque') checked @endif > Sem destaque</label>
            </div>

            <div class="form-group">
              <label for="inputData">Data</label>
              <input type="text" name="data" class="form-control datepicker" id="inputData" value="{{old('data')}}">
            </div>

            <div class="form-group">
              <label for="inputTexto">Texto</label>
              <textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
            </div>

            @include('painel.partials.imagens-upload', ['path' => 'noticias'])

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.noticias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
