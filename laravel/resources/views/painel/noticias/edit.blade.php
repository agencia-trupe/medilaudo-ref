@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Notícia</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.noticias.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

			    <div class="form-group">
						<label for="inputChamada">Chamada</label>
						<textarea name="chamada" class="form-control" id="inputChamada">{{$registro->chamada}}</textarea>
					</div>

          <div class="well" id="accordion">

            <label><input type="radio" name="tipo_destaque" value="destaque_video" @if($registro->tipo_destaque == 'destaque_video') checked @endif > Destaque em Vídeo</label>
            <div id="destaque_video" @if($registro->tipo_destaque == 'destaque_video') style="display:block;" @else style="display:none;" @endif >
              <div class="form-group">
                @if($registro->destaque_video)
                  {!! youtube_embed_link($registro->destaque_video, 0) !!}
                @endif
                <input type="text" name="destaque_link_video" placeholder="Link do vídeo (Youtube)" class="form-control" id="inputLink" value="{{$registro->destaque_video}}">
              </div>
            </div>

            <hr>

            <label><input type="radio" name="tipo_destaque" value="destaque_imagem" @if($registro->tipo_destaque == 'destaque_imagem') checked @endif > Destaque em Imagem</label>
            <div id="destaque_img" @if($registro->tipo_destaque == 'destaque_imagem') style="display:block;" @else style="display:none;" @endif >
              <div class="form-group">
                @if($registro->destaque_imagem)
                  <img src="assets/images/noticias/{{$registro->destaque_imagem}}" class="img-thumbnail"><br><br>
                @endif
                <input type="file" class="form-control" id="inputImagem" name="destaque_imagem">
              </div>
            </div>

            <hr>

            <label><input type="radio" name="tipo_destaque" value="sem_destaque" @if($registro->tipo_destaque == 'sem_destaque') checked @endif > Sem destaque</label>
          </div>

          <div class="form-group">
            <label for="inputData">Data</label>
            <input type="text" name="data" class="form-control datepicker" id="inputData" value="{{$registro->data->format('d/m/Y')}}">
          </div>

          <div class="form-group">
            <label for="inputTexto">Texto</label>
            <textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
          </div>

          @include('painel.partials.imagens-upload', ['path' => 'noticias'])

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.noticias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
