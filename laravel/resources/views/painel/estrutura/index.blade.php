@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Estrutura</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Subtítulo</th>
              <th>Texto</th>
              <th><span class="glyphicon glyphicon-list-alt"></span></th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  {{$registro->subtitulo}}
                </td>
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
                <td>
                  <a href='painel/estrutura_topicos' title='Estrutura - Tópicos' class="btn btn-sm btn-default">Gerenciar tópicos</a>
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.estrutura.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
