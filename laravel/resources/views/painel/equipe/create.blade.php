@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Membro da Equipe</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.equipe.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
              <label for="inputCategoria">Categoria</label>
              <select class="form-control" name="categoria" required>
                <option value=""></option>
                <option value="equipe_medica" @if(old('categoria') == 'equipe_medica') selected @endif >Equipe Médica</option>
                <option value="equipe_tecnica" @if(old('categoria') == 'equipe_tecnica') selected @endif >Equipe Técnica</option>
              </select>
            </div>

            <div class="form-group">
              <label for="inputÁrea">Área de Atuação</label>
              <input type="text" name="area" class="form-control" id="inputÁrea" value="{{old('area')}}">
            </div>

            <div class="form-group">
              <label for="inputNome">Nome</label>
              <input type="text" name="nome" class="form-control" id="inputNome" value="{{old('nome')}}">
            </div>

            <div class="form-group">
              <label for="inputCargo">Cargo</label>
              <input type="text" name="cargo" class="form-control" id="inputCargo" value="{{old('cargo')}}">
            </div>

            <div class="form-group">
              <label for="inputDescritivo">Descritivo</label>
              <textarea name="descritivo" class="form-control textarea-simples" id="inputDescritivo">{{old('descritivo')}}</textarea>
            </div>

            <div class="form-group">
              @if(old('imagem'))
                Imagem atual<br>
                <img src="assets/images/equipe/{{old('imagem')}}"><br>
              @endif
              <label for="inputImagem">Imagem</label>
              <input type="file" class="form-control" id="inputImagem" name="imagem">
            </div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.equipe.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
