@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Estrutura - Tópicos</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.estrutura_topicos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" disabled value="{{$registro->titulo}}">
          </div>

			    <div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control textarea-simples" id="inputTexto">{{$registro->texto}}</textarea>
					</div>

          <div class="well">
            <div class="form-group">
              @if($registro->imagem_1)
                Imagem 1 atual<br>
                <img src="assets/images/estrutura_topicos/{{$registro->imagem_1}}">
                <hr>
                <label for="inputImagem1">Substituir Imagem 1</label>
                <input type="file" class="form-control" id="inputImagem1" name="imagem_1">
                <hr>
                <label><input type="checkbox" name="remover_imagem_1" value="1"> Remover imagem</label>
              @else
                <label for="inputImagem1">Imagem 1</label>
                <input type="file" class="form-control" id="inputImagem1" name="imagem_1">
              @endif
            </div>
          </div>

          <div class="well">
            <div class="form-group">
              @if($registro->imagem_2)
                Imagem 2 atual<br>
                <img src="assets/images/estrutura_topicos/{{$registro->imagem_2}}">
                <hr>
                <label for="inputImagem2">Substituir Imagem 2</label>
                <input type="file" class="form-control" id="inputImagem2" name="imagem_2">
                <hr>
                <label><input type="checkbox" name="remover_imagem_2" value="1"> Remover imagem</label>
              @else
                <label for="inputImagem2">Imagem 2</label>
                <input type="file" class="form-control" id="inputImagem2" name="imagem_2">
              @endif
            </div>
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.estrutura_topicos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
