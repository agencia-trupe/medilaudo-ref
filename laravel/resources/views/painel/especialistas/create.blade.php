@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>{{$laudo->titulo}} - Cadastrar Especialista</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.especialistas.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputNome">Nome</label>
  						<input type="text" name="nome" class="form-control" id="inputNome" value="{{old('nome')}}" required>
  					</div>

  			    <div class="form-group">
  			      <label for="inputCRM">CRM</label>
  			      <input type="text" name="crm" class="form-control" id="inputCRM" value="{{old('crm')}}">
  			    </div>

            <input type="hidden" name="laudos_id" value="{{$laudo->id}}">

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.especialistas.index', ['laudos_id' => $laudo->id])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
