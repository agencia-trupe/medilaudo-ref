@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>{{$registro->area->titulo}} - Editar Especialista</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.especialistas.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputNome">Nome</label>
            <input type="text" name="nome" class="form-control" id="inputNome" value="{{$registro->nome}}" required>
          </div>

			    <div class="form-group">
			      <label for="inputCRM">CRM</label>
			      <input type="text" name="crm" class="form-control" id="inputCRM" value="{{$registro->crm}}">
			    </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.especialistas.index', ['laudos_id' => $registro->area->id ])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
