@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>{{$laudo->titulo}} - Especialistas</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.laudos.index') }}" class="btn btn-sm btn-default">&larr; voltar</a>

        <a href="{{ URL::route('painel.especialistas.create', ['laudos_id' => $laudo->id]) }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Especialista</a>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Nome</th>
              <th>CRM</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($laudo->especialistas as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{$registro->nome}}</td>
                <td>{{$registro->crm}}</td>            		
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.especialistas.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.especialistas.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
