@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Empresa</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.empresa.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Subtítulo</label>
            <input type="text" name="subtitulo" class="form-control" id="inputTitulo" value="{{$registro->subtitulo}}">
          </div>

          <div class="form-group">
						<label for="inputTextoEsquerda">Texto - Coluna Esquerda</label>
						<textarea name="texto_esquerda" class="form-control" id="inputTextoEsquerda">{{$registro->texto_esquerda}}</textarea>
					</div>

			    <div class="form-group">
						<label for="inputTextoDireita">Texto - Coluna Direita</label>
						<textarea name="texto_direita" class="form-control" id="inputTextoDireita">{{$registro->texto_direita}}</textarea>
					</div>

          @include('painel.partials.imagens-upload', ['path' => 'empresa'])

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.empresa.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
