<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('painel.dashboard') }}">MediLaudo</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.dashboard')}}" title="Início">Início</a>
				</li>

				<!--RESOURCEMENU-->

				<li class="dropdown @if(preg_match('~painel.(videohome|chamadas|bannerextra*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(videohome.index*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.videohome.index')}}" title="Vídeo">Vídeo</a>
						</li>
						<li @if(preg_match('~painel.(chamadas.index*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.chamadas.index')}}" title="Chamadas">Chamadas</a>
						</li>
						<li @if(str_is('painel.bannerextra*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/bannerextra' title='Banner Extra'>Banner Extra</a>
						</li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.(empresa|equipe|motivos_hospitais|vantagens|estrutura|motivos*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Páginas <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.empresa*', Route::currentRouteName())) class='active' @endif><a href='painel/empresa' title='Empresa'>Empresa</a></li>
						<li @if(str_is('painel.estrutura*', Route::currentRouteName())) class='active' @endif><a href='painel/estrutura' title='Estrutura'>Estrutura</a></li>
						<li @if(str_is('painel.equipe*', Route::currentRouteName())) class='active' @endif><a href='painel/equipe' title='Equipe'>Equipe</a></li>
						<li @if(str_is('painel.vantagens*', Route::currentRouteName())) class='active' @endif><a href='painel/vantagens' title='Vantagens da Telerradiologia'>Vantagens da Telerradiologia</a></li>
						<li @if(str_is('painel.motivos_hospitais*', Route::currentRouteName())) class='active' @endif><a href='painel/motivos_hospitais' title='Motivos para contratar a MediLaudo'>Para Hospitais e Clínicas</a></li>
						<li @if(str_is('painel.motivos_medicos*', Route::currentRouteName())) class='active' @endif><a href='painel/motivos_medicos' title='Motivos para contratar a MediLaudo'>Para Médicos</a></li>
					</ul>
				</li>

				<li @if(preg_match('~painel.(laudos|especialistas)~', Route::currentRouteName())) class='active' @endif><a href='painel/laudos' title='Laudos'>Laudos</a></li>

				<li class="dropdown @if(preg_match('~painel.(suporte*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Suporte <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.suporte_categorias*', Route::currentRouteName())) class='active' @endif><a href='painel/suporte_categorias' title='Categorias'>Categorias</a></li>
						<li @if(str_is('painel.suporte_arquivos*', Route::currentRouteName())) class='active' @endif><a href='painel/suporte_arquivos' title='Arquivos'>Arquivos</a></li>
					</ul>
				</li>

				<li @if(str_is('painel.noticias*', Route::currentRouteName())) class='active' @endif><a href='painel/noticias' title='Notícias'>Notícias</a></li>

				<li @if(str_is('painel.faq*', Route::currentRouteName())) class='active' @endif><a href='painel/faq' title='FAQ'>FAQ</a></li>

				<li class="dropdown @if(preg_match('~painel.(contato*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contato <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.contato.*', Route::currentRouteName())) class='active' @endif><a href='painel/contato' title='Informações de Contato'>Informações de Contato</a></li>
						<li @if(str_is('painel.contatos-recebidos*', Route::currentRouteName())) class='active' @endif><a href='painel/contatos-recebidos' title='Contatos recebidos'>Contatos recebidos</a></li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>

	</div>
</nav>
