@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Banner Extra</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.bannerextra.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}" required>
  					</div>

            <div class="form-group">
              @if(old('imagem'))
                Imagem atual<br>
                <img src="assets/images/bannerextra/{{old('imagem')}}"><br>
              @endif
              <label for="inputImagem">Imagem</label>
              <input type="file" class="form-control" id="inputImagem" name="imagem">
            </div>

  			    <div class="form-group">
  			      <label for="inputLink">Link</label>
  			      <input type="text" name="link" class="form-control" id="inputLink" value="{{old('link')}}">
  			    </div>

            <div class="form-group">
              <label for="inputDataInicio">Data de Início da Exibição</label>
              <input type="text" name="data_inicio" class="form-control datepicker" id="inputDataInicio" value="{{old('data_inicio')}}" required>
            </div>

            <div class="form-group">
              <label for="inputDataTermino">Data de Término da Exibição</label>
              <input type="text" name="data_termino" class="form-control datepicker" id="inputDataTermino" value="{{old('data_termino')}}" required>
            </div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.bannerextra.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
