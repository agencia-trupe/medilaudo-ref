@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Motivo - Para Médicos</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.motivos_medicos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Texto</label>
            <input type="text" name="texto" class="form-control" id="inputTitulo" value="{{$registro->texto}}">
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.motivos_medicos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
