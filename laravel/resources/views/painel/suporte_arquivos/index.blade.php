@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Suporte - Arquivos</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.suporte_arquivos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Arquivo</a>

        <hr>

        <div class="btn-group">
          <a href="{{ URL::route('painel.suporte_arquivos.index') }}" class="btn btn-sm @if($categorias_id == '') btn-primary @else btn-default @endif ">Todas as Categorias</a>
          @foreach($categorias as $c)
            <a href="{{ URL::route('painel.suporte_arquivos.index', ['categorias_id' => $c->id ]) }}" class="btn btn-sm @if($categorias_id == $c->id) btn-primary @else btn-default @endif ">{{$c->titulo}}</a>
          @endforeach
        </div>

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela='suporte_arquivos'>

          <thead>
          	<tr>
              <th>Título</th>
              <th>Descritivo</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                  {{$registro->titulo}}
                </td>
            		<td>
                  {{ str_words(strip_tags($registro->descritivo), 15)  }}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.suporte_arquivos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.suporte_arquivos.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
