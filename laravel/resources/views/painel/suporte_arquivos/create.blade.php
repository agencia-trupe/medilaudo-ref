@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Arquivo</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.suporte_arquivos.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
              <label for="inputCategoria">Categoria</label>
              <select class="form-control" name="categorias_id" required id="inputCategoria">
                <option value=""></option>
                @forelse($categorias as $cat)
                  <option value="{{$cat->id}}" @if(old('categorias_id') == $cat->id) selected @endif >{{$cat->titulo}}</option>
                @empty
                  <option value="">Sem cadastros</option>
                @endforelse
              </select>
            </div>

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}" required>
  					</div>

  			    <div class="form-group">
  						<label for="inputDescritivo">Descritivo</label>
  						<textarea name="descritivo" class="form-control textarea-simples" id="inputDescritivo">{{old('descritivo')}}</textarea>
  					</div>

            <div class="form-group">
              <label for="inputArquivo">Arquivo</label>
              <input type="file" class="form-control" id="inputArquivo" name="arquivo" required>
            </div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.suporte_arquivos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
