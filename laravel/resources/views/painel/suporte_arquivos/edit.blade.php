@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Arquivo</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.suporte_arquivos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputCategoria">Categoria</label>
            <select class="form-control" name="categorias_id" required id="inputCategoria">
              <option value=""></option>
              @forelse($categorias as $cat)
                <option value="{{$cat->id}}" @if($registro->categorias_id == $cat->id) selected @endif >{{$cat->titulo}}</option>
              @empty
                <option value="">Sem cadastros</option>
              @endforelse
            </select>
          </div>

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}" required>
          </div>

			    <div class="form-group">
						<label for="inputDescritivo">Descritivo</label>
  					<textarea name="descritivo" class="form-control textarea-simples" id="inputDescritivo">{{$registro->descritivo}}</textarea>
					</div>

          <div class="form-group well">
            @if($registro->arquivo)
              <a href="assets/arquivos/{{$registro->arquivo}}" target="_blank" class="btn btn-sm btn-default">Arquivo Atual</a>
              <hr>
            @endif
            <label for="inputArquivo">Arquivo</label>
            <input type="file" class="form-control" id="inputArquivo" name="arquivo">
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.suporte_arquivos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
