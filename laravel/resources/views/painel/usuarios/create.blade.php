@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
  	<div class="row">
  		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

	     	<h2>Adicionar Usuário do Painel Administrativo</h2>

		    <hr>

		    @include('painel.partials.mensagens')

				<form action="{{ URL::route('painel.usuarios.store') }}" method="post">

					{!! csrf_field() !!}

			    <div class="form-group">
						<label for="inputNome">Nome</label>
						<input type="text" class="form-control" id="inputNome" name="nome" value="{{ old('nome') }}" required>
					</div>

          <div class="form-group">
						<label for="inputLogin">Login</label>
						<input type="text" class="form-control" id="inputLogin" name="login" value="{{ old('login') }}" required>
					</div>

					<div class="form-group">
						<label for="inputEmail">E-mail</label>
						<input type="email" class="form-control" id="inputEmail" name="email" value="{{ old('email') }}">
					</div>

					<div class="form-group">
						<label for="inputSenha">Senha</label>
						<input type="password" class="form-control" id="inputSenha" name="password" required>
					</div>

					<div class="form-group">
						<label for="inputConfSenha">Digite novamente a Senha</label>
						<input type="password" class="form-control" id="inputConfSenha" name="password_confirm" required>
					</div>

					<hr>

					<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

					<a href="{{ URL::route('painel.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection
