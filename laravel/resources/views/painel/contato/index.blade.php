@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Informações de Contato</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Telefone</th>
              <th>Endereço</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  {{$registro->telefone}}
                </td>
            		<td>
                  {{$registro->endereco}}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.contato.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
