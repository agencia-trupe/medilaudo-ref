@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Contatos Recebidos - {{$filtro_extenso}}</h2>

		    <hr>

		  </div>
		</div>


		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

        @if($filtro == 'medicos')

          <div class="well">
            Nome: {{$registro->nome}}<br>
            E-mail: {{$registro->email}}<br>
            Telefone: {{$registro->telefone}}<br>
            Especialidade: {{$registro->especialidade}}<br>
            Título especialista no CBR: {{$registro->titulo_cbr}}<br>
            Mensagem: {{$registro->mensagem}}<br>
            @if($registro->curriculo)
              <a href="painel/download/{{$registro->curriculo}}" style="margin-top: 6px;" class="btn btn-sm btn-default" target="_blank">Visualizar Currículo</a>
            @endif
          </div>

        @elseif($filtro == 'clinicas')

          <div class="well">
            Nome: {{$registro->nome}}<br>
            E-mail: {{$registro->email}}<br>
            Hospital: {{$registro->hospital}}<br>
            Telefone: {{$registro->telefone}}<br>
            Mensagem: {{$registro->mensagem}}
          </div>

        @elseif($filtro == 'contatos')

          <div class="well">
            Nome: {{$registro->nome}}<br>
            E-mail: {{$registro->email}}<br>
            Telefone: {{$registro->telefone}}<br>
            Mensagem: {{$registro->mensagem}}
          </div>

        @endif

			</div>
		</div>

	   <a href="{{ URL::route('painel.contatos-recebidos.index', ['filtro' => $filtro])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

  </div>

@endsection
