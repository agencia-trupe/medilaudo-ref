@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Contatos Recebidos - {{$filtro_extenso}}</h2>

        <hr>

        <div class="btn-group">
          <a href="painel/contatos-recebidos?filtro=contatos" class="btn btn-sm btn-default @if($filtro == 'contatos') btn-primary @endif ">Contatos do Site</a>
          <a href="painel/contatos-recebidos?filtro=clinicas" class="btn btn-sm btn-default @if($filtro == 'clinicas') btn-primary @endif ">Hospitais e Clínicas</a>
          <a href="painel/contatos-recebidos?filtro=medicos" class="btn btn-sm btn-default @if($filtro == 'medicos') btn-primary @endif ">Médicos</a>
        </div>

      	<table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Nome</th>
              <th>E-mail</th>
              <th>Data</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  {{$registro->nome}}
                </td>
            		<td>
                  {{$registro->email}}
                </td>
                <td>
                  {{$registro->created_at->format('d/m/Y H:i:s')}}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.contatos-recebidos.show', ['id' => $registro->id, 'filtro' => $filtro] ) }}" class="btn btn-primary btn-sm">editar</a>
                  <form action="{{ URL::route('painel.contatos-recebidos.destroy', ['id' => $registro->id, 'filtro' => $filtro]) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
