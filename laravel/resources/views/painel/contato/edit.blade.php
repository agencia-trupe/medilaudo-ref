@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Informações de Contato</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.contato.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTelefone">Telefone</label>
            <input type="text" name="telefone" class="form-control" id="inputTelefone" value="{{$registro->telefone}}">
          </div>

          <div class="form-group">
            <label for="inputEndereco">Endereco</label>
            <input type="text" name="endereco" class="form-control" id="inputEndereco" value="{{$registro->endereco}}">
          </div>

          <div class="form-group">
            <label for="inputGoogleMaps">Código de incorporação do Google Maps</label>
            <input type="text" name="google_maps" class="form-control" id="inputGoogleMaps" value="{{$registro->google_maps}}">
          </div>

          <div class="form-group">
            <label for="inputLinkedin">LinkedIn</label>
            <input type="text" name="linkedin" class="form-control" id="inputLinkedin" value="{{$registro->linkedin}}">
          </div>

          <div class="form-group">
            <label for="inputFacebook">Facebook</label>
            <input type="text" name="facebook" class="form-control" id="inputFacebook" value="{{$registro->facebook}}">
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
