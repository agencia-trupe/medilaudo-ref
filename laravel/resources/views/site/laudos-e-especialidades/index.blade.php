@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-laudos">

    <div class="centralizar">

      <h1>LAUDOS & ESPECIALIDADES</h1>

      <h2>A MediLaudo oferece diversas especialidades para a sua clínica ou hospital</h2>

      <p class="chamada">
        Confira as áreas de atuação e os médicos especialistas de cada uma delas.
        A MediLaudo possui uma equipe completa de médicos especialistas em suas
        atividades. Seus laudos estarão em excelentes mãos!
      </p>

    </div>

    <ul class="lista-laudos">
      @foreach($laudos as $laudo)
        <li class="tooltip" data-tooltip-content="#laudo-{{md5($laudo->id)}}">
          {{$laudo->titulo}}
          {{--
          <div class="tooltip-conteudo" id="laudo-{{md5($laudo->id)}}">
            @forelse($laudo->especialistas as $medico)
              <p class='lista-medico'>
                {{$medico->nome}}<br>
                CRM {{$medico->crm}}
              </p>
            @empty
              Aguardando Cadastro
            @endforelse
          </div>
           --}}
        </li>
      @endforeach
    </ul>

    <h3 class="titulo-vantagem">VANTAGENS DA TELERRADIOLOGIA</h3>

    <div class="vantagens">
      <div class="centralizar">
        @foreach($vantagens as $vantagem)
          <div class="vantagem">
            <h4>{{$vantagem->titulo}}</h4>
            <div class="cke">
              {!! $vantagem->texto !!}
            </div>
          </div>
        @endforeach
      </div>
    </div>


  </div>

@stop
