@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-suporte">
    <div class="centralizar">

      <h1>SUPORTE</h1>

      <div class="lista-categorias">
        @foreach($categorias as $k => $cat)
          @if($k > 0) &nbsp;&bull;&nbsp; @endif
          <a href="suporte/{{$cat->slug}}" title="{{$cat->titulo}}" @if($cat->slug == $slug_categoria) class="ativo" @endif >{{$cat->titulo}}</a>
        @endforeach
      </div>

      <div class="lista-arquivos">
        <div class="link">
          <a href="https://get.teamviewer.com/medilaudo_suporte" target="_blank" title="Suporte a distância">
            <h2>Suporte a distância</h2>
            <p></p>
          </a>
        </div>
        @foreach($arquivos as $arquivo)
          <div class="link">
            <a href="suporte/download/{{$arquivo->id.'-'.str_slug($arquivo->titulo)}}" title="{{$arquivo->titulo}}">
              <h2>
                {{$arquivo->titulo}}
              </h2>
              <p>{{$arquivo->descritivo}}</p>
            </a>
          </div>
        @endforeach
      </div>

    </div>
  </div>

@stop
