@extends('site.template.index')

@section('conteudo')

  <div class="video-home">

    <div class="centralizar">
      <div class="video-container">
        @if($videohome->link_video)

          <div id="muteYouTubeVideoPlayer"></div>

          <script async src="https://www.youtube.com/iframe_api"></script>
          <script>
           function onYouTubeIframeAPIReady() {
            var player;
            player = new YT.Player('muteYouTubeVideoPlayer', {
              videoId: '{{youtube_id_from_url($videohome->link_video)}}', // YouTube Video ID
              width: 560,               // Player width (in px)
              height: 316,              // Player height (in px)
              playerVars: {
                autoplay: 1,        // Auto-play the video on load
                controls: 1,        // Show pause/play buttons in player
                showinfo: 0,        // Hide the video title
                modestbranding: 1,  // Hide the Youtube Logo
                loop: 1,            // Run the video in a loop
                fs: 0,              // Hide the full screen button
                cc_load_policy: 1, // Hide closed captions
                iv_load_policy: 3,  // Hide the Video Annotations
                autohide: 0         // Hide video controls when playing
              },
              events: {
                onReady: function(e) {
                  e.target.mute();
                }
              }
            });
           }
          </script>
        @endif
      </div>
    </div>

  </div>

  <div class="chamada">

    <div class="centralizar">
      <h3>A TELERRADIOLOGIA PODE REVOLUCIONAR A PRESTAÇÃO DE SERVIÇOS DO SEU HOSPITAL OU CLÍNICA</h3>
      <div class="lista-chamadas">
        @foreach($chamadas as $chamada)
          <a href="{{$chamada->link}}" title="{{$chamada->texto}}">
            <p>{{$chamada->texto}}</p>
            <div class="seta"><img src="assets/images/layout/seta-caixa-torta.png" alt=""></div>
            <div class="fundo-skew"></div>
          </a>
        @endforeach
      </div>
    </div>

    @if(count($bannerExtra))
      <div class="banner-extra">
        <div class="centralizar">
          <h4>ACOMPANHE A MEDILAUDO</h4>
          @foreach($bannerExtra as $banner)
            <a href="{{$banner->link}}" title="{{$banner->titulo}}">
              <h3>{{$banner->titulo}}</h3>
              <img src="assets/images/bannerextra/{{$banner->imagem}}" alt="{{$banner->titulo}}">
            </a>
          @endforeach
        </div>
      </div>
    @endif

  </div>

  <div class="como-funciona">
    <h3>COMO FUNCIONA A TELERRADIOLOGIA DA MEDILAUDO</h3>
    <img src="assets/images/layout/infografico-comofunciona.png" alt="COMO FUNCIONA A TELERRADIOLOGIA DA MEDILAUDO">
  </div>

  <div class="laudos">
    <h3>OBTENHA LAUDOS DE DIAGNÓSTICO POR IMAGEM EM POUCAS HORAS:</h3>
    <ul>
      @foreach($listaLaudos as $laudo)
        <li>
          <span>{{$laudo->titulo}}</span>
        </li>
      @endforeach
    </ul>
  </div>

  <div class="links">

    <h3>Você pode ingressar na Telerradiologia agora:</h3>

    <a href="para-hospitais-e-clinicas#form-contato" class="hospitais" title="CREDENCIE SUA CLÍNICA OU HOSPITAL">
      <div class="texto">
        <h4><span>CLÍNICAS, LABORATÓRIOS E HOSPITAIS</span> <img src="assets/images/layout/circulo-play.png"></h4>
        <h5>CREDENCIE SUA CLÍNICA OU HOSPITAL: CLIQUE E RECEBA SUA PROPOSTA</h5>
      </div>
    </a>

    <a href="para-medicos#form-contato" class="medicos" title="VENHA LAUDAR CONOSCO">
      <div class="texto">
        <h4><span>PROFISSIONAIS DE MEDICINA</span> <img src="assets/images/layout/circulo-play.png"></h4>
        <h5>MÉDICO ESPECIALISTA: VENHA LAUDAR CONOSCO - DO BRASIL OU EXTERIOR</h5>
      </div>
    </a>

  </div>

@stop
