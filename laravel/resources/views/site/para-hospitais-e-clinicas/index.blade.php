@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-vantagens">
    <div class="centralizar">

      <h1>PARA HOSPITAIS E CLÍNICAS</h1>

      <h2>Sua empresa só tem vantagens com a telerradiologia</h2>

      <h3>{{count($motivos)}} motivos para contratar a MediLaudo</h3>

      <div class="lista-vantagens">
        @foreach($motivos as $k => $motivo)
          <div class="item">
            <div class="contador">{{ ($k + 1) }}</div>
            <div class="texto">{{$motivo->texto}}</div>
          </div>
        @endforeach
      </div>

      <div class="form form-hospitais">

        @if($errors->any())
          <p class="erro" id="resposta-form">
            {{$errors->first()}}
          </p>
        @endif

        @if(session('contato_enviado'))
          <p class="sucesso" id="resposta-form">
            Sua mensagem foi enviada com sucesso!
          </p>
        @endif

        <form action="{{URL::route('site.para-hospitais-e-clinicas.enviar')}}" method="post" id="form-contato">
          {!! csrf_field() !!}

          <h1>CLÍNICAS, LABORATÓRIOS E HOSPITAIS</h1>

          <h2>CREDENCIE SUA CLÍNICA OU HOSPITAL E RECEBA SUA PROPOSTA</h2>

          <div class="form-group">
            <input type="text" name="nome" placeholder="nome" value="{{old('nome')}}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{old('email')}}" required>
            <input type="text" name="hospital" placeholder="hospital ou clínica" value="{{old('hospital')}}" required>
            <input type="text" name="telefone" placeholder="telefone" value="{{old('telefone')}}" required>
            <textarea name="mensagem" placeholder="mensagem (opcional)">{{old('mensagem')}}</textarea>
            <div class="submit">
              <div class="button">
                <input type="submit" value="enviar" title="Enviar">
              </div>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>

@stop
