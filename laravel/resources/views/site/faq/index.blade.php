@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-faq">

    <h1>PERGUNTAS FREQUENTES</h1>

    <h2>Tire todas as suas dúvidas sobre a Telerradiologia no Brasil</h2>

    <ul class="lista-faq">
      @foreach($faq as $k => $item)
        <li>
          <div class="centralizar">
            <div class="questao">
              <div class="contador">{{($k + 1)}}</div>
              <h3>{{$item->titulo}}</h3>
            </div>
            <div class="resposta cke">
              {!! $item->texto !!}
            </div>
          </div>
        </li>
      @endforeach
    </ul>

  </div>
@stop
