@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-vantagens">
    <div class="centralizar">

      <h1>PARA MÉDICOS</h1>

      <h2>Venha laudar com a MediLaudo!</h2>

      <p class="chamada">
        Os médicos especialistas da MediLaudo têm à sua disposição um sistema de produção
        que permite o controle autônomo de suas tarefas, um espaço totalmente equipado
        ou a opção de trabalharem a partir de qualquer lugar com acesso à internet.
        Faça parte desta equipe de sucesso!
      </p>

      <div class="lista-vantagens">
        @foreach($motivos as $k => $motivo)
          <div class="item">
            <div class="contador">{{ ($k + 1) }}</div>
            <div class="texto">{{$motivo->texto}}</div>
          </div>
        @endforeach
      </div>

      <div class="form form-medicos">

        @if($errors->any())
          <p class="erro" id="resposta-form">
            {{$errors->first()}}
          </p>
        @endif

        @if(session('contato_enviado'))
          <p class="sucesso" id="resposta-form">
            Sua mensagem foi enviada com sucesso!
          </p>
        @endif

        <form action="{{URL::route('site.para-medicos.enviar')}}" method="post" enctype="multipart/form-data" id="form-contato">
          {!! csrf_field() !!}

          <h1>PROFISSIONAIS DE MEDICINA</h1>

          <h2>MÉDICO ESPECIALISTA: ENVIE SEU CURRÍCULO</h2>

          <div class="form-group">
            <input type="text" name="nome" placeholder="nome" value="{{old('nome')}}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{old('email')}}" required>
            <input type="text" name="telefone" placeholder="telefone" value="{{old('telefone')}}" required>
            <input type="text" name="especialidade" placeholder="especialidade" value="{{old('especialidade')}}" required>
            <input type="text" name="titulo_cbr" placeholder="título especialista no CBR" value="{{old('titulo_cbr')}}" required>
            <textarea name="mensagem" placeholder="mensagem (opcional)">{{old('mensagem')}}</textarea>
            <div class="cv">
              <label for="inputArquivo">ANEXAR CURRÍCULO</label>
              <input type="file" name="arquivo" id="inputArquivo" required>
            </div>
            <div class="submit">
              <div class="button">
                <input type="submit" value="enviar" title="Enviar">
              </div>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>

@stop
