@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-contato">

    <h1>CONTATO</h1>

    <div class="faixa-verde">
      <div class="telefone">
        {{$contato->telefone}}
      </div>
      <div class="endereco">
        {{$contato->endereco}}
      </div>
    </div>

    <div class="colunas">
      <div class="coluna">
        <div class="maps-container">
          {!! $contato->google_maps !!}
        </div>
      </div>
      <div class="coluna">
        <form action="contato" method="post">
          {!! csrf_field() !!}
          <h2>FALE CONOSCO</h2>

          @if($errors->any())
            <p class="erro" id="resposta-form">
              {{$errors->first()}}
            </p>
          @endif

          @if(session('contato_enviado'))
            <p class="sucesso" id="resposta-form">
              Sua mensagem foi enviada com sucesso!
            </p>
          @endif

          <input type="text" name="nome" placeholder="nome" value="{{old('nome')}}" required>
          <input type="email" name="email" placeholder="e-mail" value="{{old('email')}}" required>
          <input type="text" name="telefone" placeholder="telefone" value="{{old('telefone')}}">
          <textarea name="mensagem" placeholder="mensagem" required>{{old('mensagem')}}</textarea>
          <input type="submit" value="ENVIAR">
        </form>
      </div>
    </div>

  </div>
@stop
