<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2017 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta name="keywords" content="telerradiologia, telemedicina, laudos à distância, laudos via internet, telerradiologia são paulo, telerradiologia sp, diagnóstico remoto, laudos remotos, laudos de especialistas, medicina radiologia" />

  <title>MediLaudo</title>

  <meta name="title" content="MediLaudo Telerradiologia - Laudos à distância" />
  <meta name="description" content="Telerradiologia. Laudos de especialistas a um clique de distância da sua empresa." />
  <meta name="url" content="http://www.medilaudo.net" />

  <base href="{{ base_url() }}">
  <meta property="og:description" content="Telerradiologia. Laudos de especialistas a um clique de distância da sua empresa."/>
  <meta property="og:title" content="MediLaudo Telerradiologia - Laudos à distância" />
  <meta property="og:url" content="http://www.medilaudo.net" />
  <meta property="og:site_name" content="MediLaudo Telerradiologia"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="http://medilaudo.net/assets/images/layout/logo-medilaudo.png"/>


  <link href="assets/css/site/main.css" rel="stylesheet" type="text/css" />

  <script type="text/javascript" src="assets/vendor/jquery.js"> </script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96448840-1', 'auto');
  ga('send', 'pageview');
  </script>
</head>
<body>

  @include('site.partials.header')

  @include('site.partials.menu')

  @yield('conteudo')

  @include('site.partials.footer')


</body>
</html>
