@extends('site.template.index')

@section('conteudo')

    <div class="conteudo conteudo-noticias">
      <div class="centralizar">

        <h1>NOTÍCIAS</h1>

        <h2>Novidades da área de Telerradiologia e diagnósticos</h2>

        <div class="detalhes-noticia">

          <div class="relacionadas">
            @foreach($noticias as $c => $noticia)
              <a href="noticias/ler/{{$noticia->slug}}" title="{{$noticia->titulo}}" @if($c <= 2) class="destaque" @endif>
                <div class="link">
                  <h3>{{$noticia->titulo}}</h3>
                  <p>{!! $noticia->chamada !!}</p>
                  @if($c <= 2)

                    @if($noticia->tipo_destaque == 'destaque_imagem')
                      <div class="midia-destaque">
                        <img src="assets/images/noticias/{{$noticia->destaque_imagem}}" alt="{{$noticia->titulo}}">
                      </div>
                    @endif

                    @if($noticia->tipo_destaque == 'destaque_video')
                      <div class="midia-destaque em-video">
                        <img src="assets/images/noticias/destaques/{{$noticia->thumb_video}}" alt="{{$noticia->titulo}}">
                      </div>
                    @endif

                  @endif
                </div>
              </a>
            @endforeach
          </div>

          <div class="detalhes">
            <h1>{{$detalhe->titulo}}</h1>

            @if($detalhe->tipo_destaque == 'destaque_imagem')
              <div class="midia-destaque">
                <img src="assets/images/noticias/{{$detalhe->destaque_imagem}}" alt="{{$detalhe->titulo}}">
              </div>
            @endif

            @if($detalhe->tipo_destaque == 'destaque_video')
              <div class="midia-destaque em-video">
                <div class="video-container">
                  {!! youtube_embed_link($detalhe->destaque_video, 0) !!}
                </div>
              </div>
            @endif

            <div class="texto cke">
              {!! $detalhe->texto !!}
            </div>

            @if(sizeof($imagens))
              <div class="lista-imagens">
                @foreach($imagens as $img)
                  <a href="assets/images/noticias/redimensionadas/{{$img->imagem}}" title="Ampliar imagem" rel="galeria">
                    <img src="assets/images/noticias/thumbs-quadradas/{{$img->imagem}}">
                  </a>
                @endforeach
              </div>
            @endif

            <div class="navegacao">
              @if($anterior)
                <a href="noticias/ler/{{$anterior->slug}}" title="Matéria Anterior">&laquo; matéria anterior</a>
              @endif
              @if($proximo)
                <a href="noticias/ler/{{$proximo->slug}}" title="Próxima Matéria">próxima matéria &raquo;</a>
              @endif
            </div>
          </div>

        </div>


      </div>
    </div>

@stop
