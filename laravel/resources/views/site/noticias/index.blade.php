@extends('site.template.index')

@section('conteudo')

    <div class="conteudo conteudo-noticias">
      <div class="centralizar">

        <h1>NOTÍCIAS</h1>

        <h2>Novidades da área de Telerradiologia e diagnósticos</h2>

        <div class="lista-noticias">

          @foreach($destaques as $destaque)
            <a href="noticias/ler/{{$destaque->slug}}" title="{{$destaque->titulo}}" class="destaque">
              <div class="link">
                <h3>{{$destaque->titulo}}</h3>
                <p>{!! $destaque->chamada !!}</p>

                @if($destaque->tipo_destaque == 'destaque_imagem')
                  <div class="midia-destaque">
                    <img src="assets/images/noticias/{{$destaque->destaque_imagem}}" alt="{{$destaque->titulo}}">
                  </div>
                @endif

                @if($destaque->tipo_destaque == 'destaque_video')
                  <div class="midia-destaque em-video">
                    <img src="assets/images/noticias/destaques/{{$destaque->thumb_video}}" alt="{{$destaque->titulo}}">
                  </div>
                @endif

              </div>
            </a>
          @endforeach

          @foreach($noticias as $noticia)
            <a href="noticias/ler/{{$noticia->slug}}" title="{{$noticia->titulo}}">
              <div class="link">
                <h3>{{$noticia->titulo}}</h3>
              </div>
            </a>
          @endforeach

        </div>

      </div>
    </div>

@stop
