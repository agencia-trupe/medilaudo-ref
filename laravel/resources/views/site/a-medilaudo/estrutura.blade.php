@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-estrutura">
    <div class="centralizar">

      <h1>ESTRUTURA</h1>

      <h2>{{$texto->subtitulo}}</h2>

      <div class="colunas">
        <div class="coluna">
          <div class="imagens">
            @foreach($imagens as $c => $img)
              <div class="imagem">
                <img src="assets/images/estrutura/thumbs-retangulares/{{$img->imagem}}">
              </div>
            @endforeach
          </div>
        </div>
        <div class="coluna cke">
          {!! $texto->texto !!}

          <div class="topicos">
            @foreach($topicos as $c => $topico)
              @if($c == 2)

                <div class="topico destaque">
                  <div class="imagem">
                    <img src="assets/images/layout/icone_informatica.png">
                  </div>
                  <div class="texto">
                    <h3>{{$topico->titulo}}</h3>
                    <p>{{$topico->texto}}</p>
                  </div>
                  <div class="topicos-imagens">
                    @if($topico->imagem_1) <div class="imagem-topico"><img src="assets/images/estrutura_topicos/{{$topico->imagem_1}}"></div> @endif
                    @if($topico->imagem_2) <div class="imagem-topico"><img src="assets/images/estrutura_topicos/{{$topico->imagem_2}}"></div> @endif
                  </div>
                </div>

              @else

                <div class="topico">
                  <div class="imagem">
                    @if($topico->imagem_1) <div class="imagem-topico"><img src="assets/images/estrutura_topicos/thumbs/{{$topico->imagem_1}}"></div> @endif
                  </div>
                  <div class="texto">
                    <h3>{{$topico->titulo}}</h3>
                    <p>{{$topico->texto}}</p>
                  </div>
                </div>

              @endif
            @endforeach
          </div>

        </div>
      </div>

    </div>
  </div>

@stop
