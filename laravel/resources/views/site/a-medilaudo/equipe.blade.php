@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-equipe">
    <div class="centralizar">

      <h1>EQUIPE MÉDICA</h1>

      <h2>Um time de especialistas médicos garantem o seu laudo em poucas horas</h2>

      <div class="lista-equipe">
        @foreach($equipe_medica as $c => $membro)
          <div class="membro @if($c%2==0) par @else impar @endif ">

            @if($c%2==0)

              <div class="imagem">
                @if($membro->imagem)
                  <img src="assets/images/equipe/{{$membro->imagem}}" alt="{{$membro->nome}}">
                @endif
              </div>
              <div class="texto">
                <div class="area">{{$membro->area}}</div>
                <div class="nome">{{$membro->nome}}</div>
                <div class="cargo">{{$membro->cargo}}</div>
                <div class="descritivo">{!! nl2br($membro->descritivo) !!}</div>
              </div>

            @else

              <div class="texto">
                <div class="area">{{$membro->area}}</div>
                <div class="nome">{{$membro->nome}}</div>
                <div class="cargo">{{$membro->cargo}}</div>
                <div class="descritivo">{!! nl2br($membro->descritivo) !!}</div>
              </div>
              <div class="imagem">
                @if($membro->imagem)
                  <img src="assets/images/equipe/{{$membro->imagem}}" alt="{{$membro->nome}}">
                @endif
              </div>

            @endif

          </div>
        @endforeach
      </div>

      <h1>EQUIPE TÉCNICA</h1>

      <div class="lista-equipe">
        @foreach($equipe_tecnica as $c => $membro)
          <div class="membro @if($c%2==0) par @else impar @endif ">

            @if($c%2==0)

              <div class="imagem">
                @if($membro->imagem)
                  <img src="assets/images/equipe/{{$membro->imagem}}" alt="{{$membro->nome}}">
                @endif
              </div>
              <div class="texto">
                <div class="area">{{$membro->area}}</div>
                <div class="nome">{{$membro->nome}}</div>
                <div class="cargo">{{$membro->cargo}}</div>
                <div class="descritivo">{!! nl2br($membro->descritivo) !!}</div>
              </div>

            @else

              <div class="texto">
                <div class="area">{{$membro->area}}</div>
                <div class="nome">{{$membro->nome}}</div>
                <div class="cargo">{{$membro->cargo}}</div>
                <div class="descritivo">{!! nl2br($membro->descritivo) !!}</div>
              </div>
              <div class="imagem">
                @if($membro->imagem)
                  <img src="assets/images/equipe/{{$membro->imagem}}" alt="{{$membro->nome}}">
                @endif
              </div>

            @endif

          </div>
        @endforeach
      </div>

    </div>
  </div>

@stop
