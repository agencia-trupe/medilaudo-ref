@extends('site.template.index')

@section('conteudo')

  <div class="conteudo conteudo-empresa">
    <div class="centralizar">

      <h1>EMPRESA</h1>

      <h2>{{$texto->subtitulo}}</h2>

      <div class="colunas">
        <div class="coluna cke">
          {!! $texto->texto_esquerda !!}
        </div>
        <div class="coluna cke">
          {!! $texto->texto_direita !!}
        </div>
      </div>

      <div class="imagens">
        @foreach($imagens as $c => $img)
          <div class="imagem">
            <img src="assets/images/empresa/thumbs-retangulares/{{$img->imagem}}">            
          </div>
        @endforeach
      </div>

    </div>
  </div>

@stop
