<nav>
  <div class="centralizar">
    <ul>
      <li><a href="#" title="MENU" id="toggle-menu">MENU</a></li>
      <li><a href="a-medilaudo" title="A MEDILAUDO" @if(preg_match('~a-medilaudo~', Route::currentRouteName())) class="ativo" @endif >A MEDILAUDO</a></li>
      <li><a href="laudos-e-especialidades" title="LAUDOS & ESPECIALIDADES" @if(preg_match('~laudos-e-especialidades~', Route::currentRouteName())) class="ativo" @endif >LAUDOS & ESPECIALIDADES</a></li>
      <li><a href="para-hospitais-e-clinicas" title="PARA HOSPITAIS E CLÍNICAS" @if(preg_match('~para-hospitais-e-clinicas~', Route::currentRouteName())) class="ativo" @endif >PARA HOSPITAIS E CLÍNICAS</a></li>
      <li><a href="para-medicos" title="PARA MÉDICOS" @if(preg_match('~para-medicos~', Route::currentRouteName())) class="ativo" @endif >PARA MÉDICOS</a></li>
      <li><a href="suporte" title="SUPORTE" @if(preg_match('~suporte~', Route::currentRouteName())) class="ativo" @endif >SUPORTE</a></li>
      <li><a href="noticias" title="NOTÍCIAS" @if(preg_match('~noticias~', Route::currentRouteName())) class="ativo" @endif >NOTÍCIAS</a></li>
      <li><a href="faq" title="FAQ" @if(preg_match('~faq~', Route::currentRouteName())) class="ativo" @endif >FAQ</a></li>
      <li><a href="contato" title="CONTATO" @if(preg_match('~contato~', Route::currentRouteName())) class="ativo" @endif >CONTATO</a></li>
    </ul>
  </div>
</nav>

@if(preg_match('~a-medilaudo~', Route::currentRouteName()))
  <div id="submenu">
    <div class="centralizar">
      <ul>
        <li><a href="a-medilaudo" title="EMPRESA" @if(preg_match('~a-medilaudo.index~', Route::currentRouteName())) class="ativo" @endif >EMPRESA</a></li>
        <li><a href="a-medilaudo/estrutura" title="ESTRUTURA" @if(preg_match('~a-medilaudo.estrutura~', Route::currentRouteName())) class="ativo" @endif >ESTRUTURA</a></li>
        <li><a href="a-medilaudo/equipe" title="EQUIPE" @if(preg_match('~a-medilaudo.equipe~', Route::currentRouteName())) class="ativo" @endif >EQUIPE</a></li>
      </ul>
    </div>
  </div>
@endif
