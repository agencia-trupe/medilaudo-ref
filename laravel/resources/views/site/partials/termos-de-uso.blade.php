<h1>
  Termos de uso
</h1>
<p>
  A sua privacidade é muito importante para a <strong>MEDILAUDO</strong>. Por
    isso, criamos uma Política de Privacidade que abrange a forma como
    coletamos, usamos, divulgamos, transferimos e armazenamos suas informações.
</p>
<p>
  É importante que você leia. Se tiver dúvida, entre em contato conosco.
</p>
<p>
  <strong>Captação e uso de dados pessoais</strong>
</p>
<p>
  Os dados pessoais podem ser usados para identificar ou contatar o paciente.
Eles podem ser solicitados a qualquer momento em contato com a    <strong>MEDILAUDO</strong>, seja por meio do site, aplicativo, telefone ou
    no centro de imagem.
</p>
<p>
  A MediLaudo somente utilizará essas informações pessoais de maneira
    consistente com o indicado nesta Política de Privacidade.
</p>
<p>
  As informações coletadas também podem ser analisadas e associadas a outras
    informações a que a MediLaudo tiver acesso, garantindo a melhoraria de
    nossos produtos, serviços, conteúdo e propaganda, sempre respeitando a
    presente Política de Privacidade.
</p>
<p>
  Não é obrigatório que você forneça as informações pessoais que pedimos,
    porém sem esses dados, em muitos casos, não conseguiremos oferecer a você
    nossos produtos e serviços ou responder as suas dúvidas.
</p>
<p>
  Aqui estão alguns exemplos dos tipos de dados pessoais que a MediLaudo pode
    pedir e como podemos usá-las.
</p>
<p>
  <strong>Dados pessoais solicitados</strong>
</p>
<p>
  Quando você cria uma conta de acesso no site ou aplicativo da MediLaudo ou,
    ainda, realiza um exame em um centro de imagem, podemos coletar informações
    suas, as quais incluem, mas não se limitam, ao seu nome, endereço, CPF (ou
    outro número de documento), telefone e e-mail.
</p>
<p>
  <strong>Como usamos seus dados pessoais</strong>
</p>
<p>
  As informações pessoais que coletamos nos permite manter você atualizado
    sobre os mais recentes anúncios de produtos, atualizações de serviço e de
    eventos da MediLaudo. Se não quiser participar de nossa lista, poderá optar
    por não receber nossa comunicação a qualquer momento, seja no cadastro ou
    atualizando suas preferências.
</p>
<p>
  As informações pessoais também são utilizadas para criar, desenvolver,
    operar, fornecer e melhorar nossos produtos, serviços, conteúdo e
    publicidade, além de prevenir perdas e evitar fraudes.
</p>
<p>
  De tempos em tempos poderemos usar suas informações pessoais para enviar
    avisos importantes, como comunicações sobre os serviços e alterações em
    nossos termos, condições e procedimentos.
</p>
<p>
  Podemos também usar informações pessoais para propósitos internos, como
    auditoria, análise de dados e pesquisas visando a melhoria dos produtos,
    serviços e a comunicação com os pacientes da MediLaudo.
</p>
<p>
  <strong>Coleta e uso de dados não pessoais</strong>
</p>
<p>
  Também coletamos dados que, analisados de forma independente, não permitem
    a associação direta a um indivíduo específico. Podemos coletar, usar,
    transferir e revelar informações não pessoais para qualquer propósito. Veja
    alguns exemplos de dados não pessoais que captamos e como podemos usá-las.
</p>
<p>
  Podemos coletar informações como ocupação, idioma, código postal, código de
    área, identificador único de dispositivo, localização e fuso horário em que
    um produto MediLaudo é usado para que possamos entender melhor o
    comportamento do cliente e melhorar nossos produtos, serviços e propaganda.
</p>
<p>
  Também coletamos informações sobre atividades do cliente e usuários em
    nossos sites, produtos e serviços. Essas informações são utilizadas com o
    propósito de nos auxiliar no fornecimento de produtos e serviços mais úteis
    e completos aos nossos clientes e compreender quais de suas partes geram
    maior interesse.
</p>
<p>
  Se nós juntarmos as informações não pessoais com informações pessoais, as
    informações combinadas serão tratadas como informações pessoais enquanto
    permanecerem combinadas.
</p>
<p>
  <strong>Outros usos dos dados coletados</strong>
</p>
<p>
  Independentemente de se tratar de informações pessoais ou não pessoais, a
    MediLaudo poderá utilizar os dados e informações coletados para as
    seguintes finalidades:
</p>
<ul>
    <li>

          Responder a eventuais dúvidas e solicitações do cliente;

    </li>
    <li>

          Cumprimento de ordem legal ou judicial;

    </li>
    <li>

          Constituir, defender ou exercer regularmente direitos em âmbito
            judicial ou administrativo;

    </li>
    <li>

          Garantir a segurança dos pacientes;

    </li>
    <li>

          Manter atualizados os cadastros dos pacientes para contato por
            telefone fixo, celular, email, SMS, mala direta, redes sociais ou
            por outros meios de comunicação;

    </li>
    <li>

          Promover o serviço e seus parceiros, comerciais ou não, e informar
            sobre novas oportunidades e benefícios para o paciente

    </li>
    <li>

          Gerar análises e estudos, sejam estatísticos ou identificáveis, com
            base no comportamento de uso das ferramentas, site, produtos e
            serviços da MediLaudo.

    </li>
    <li>

          Promover o serviço e seus parceiros, comerciais ou não, e informar
            sobre novas oportunidades e benefícios para o cliente;

    </li>
    <li>

          Gerar análises e estudos, sejam estatísticos ou identificáveis, com
            base no comportamento de uso das ferramentas, site, produtos e
            serviços da MediLaudo;

    </li>
    <li>

          Aperfeiçoar o uso e a experiência interativa durante a navegação do
            paciente no site, produtos e serviços da MediLaudo bem como das
            demais ferramentas e plataformas lançadas pela MediLaudo; e Os
            dados adquiridos somente poderão ser acessados por profissionais
            devidamente autorizados pela MediLaudo, respeitando a necessidade a
            que serão submetidos, a relevância para os objetivos da MediLaudo e
            os interesses dos usuários, além de preservar sua privacidade.

    </li>
</ul>
<p>
  <strong>Cookies e outras tecnologias</strong>
</p>
<p>
  O site, os serviços on-line, os aplicativos interativos, as mensagens de
    e-mail e as propagandas da MediLaudo podem usar “cookies” e outras
    tecnologias, como pixel tags e web beacons.
</p>
<p>
  Essas tecnologias nos ajudam a entender melhor o comportamento do paciente,
    nos informando que partes de nosso site as pessoas visitaram, além de
    ajudar e medir a eficácia das propagandas e pesquisas na web. As
    informações coletadas por cookies e outras tecnologias são tratadas como
    informações não pessoais, todavia, à medida que os endereços do Protocolo
    da Internet (IP, Internet Protocol) ou identificadores similares sejam
    considerados informações pessoais pela lei local, também trataremos esses
    identificadores como informações pessoais. Similarmente, à medida que as
    informações não pessoais sejam combinadas com informações pessoais, as
    informações combinadas são tratadas como informações pessoais para os
    propósitos desta Política de Privacidade.
</p>
<p>
  A MediLaudo e seus parceiros usam cookies e outras tecnologias de serviços
    de publicidade móvel para controlar o número de exibições de determinado
    anúncio, mostrar anúncios de acordo com os interesses do paciente e avaliar
    o desempenho das campanhas de publicidade.
</p>
<p>
  Quando você usa um dispositivo móvel para acessar os Serviços, é possível
    limitar as informações que podem ser recolhidas pela MediLaudo e por
    terceiros. Os sistemas operacionais dos dispositivos móveis são diferentes,
    por isso, confira o menu “Configuração” de dispositivo móvel para saber
    como alterar suas preferências relacionadas a “Limitar Publicidade
    Rastreada”. Se optar por ativar essa função você continuará a receber o
    mesmo número de anúncios móveis, mas eles poderão ser menos relevantes, uma
    vez que não serão baseados nos seus interesses.
</p>
<p>
  Informamos que a desativação por meio do link fornecido aplica-se apenas
    aos serviços de publicidade da MediLaudo e não influencia anúncios de
    outras redes de publicidade, portanto você ainda pode ver anúncios
    personalizados e direcionados em páginas da web ou aplicativos. No entanto,
    se a opção “Limitar Publicidade Rastreada” estiver ativa em seu dispositivo
    móvel, não apenas o MediLaudo como também terceiros não poderão usar o
    “Identificador de Publicidade”, que possui a função de auxiliar na
    apresentação de anúncios direcionados, resultando na apresentação apenas de
    anúncios não direcionados.
</p>
<p>
  A MediLaudo e seus parceiros também utilizam cookies e outras tecnologias
    para coletar e armazenar informações pessoais quando nossos produtos,
    serviços, aplicativos e site são utilizados. Nossa meta nesses casos é
    tornar sua experiência com a MediLaudo mais conveniente e pessoal. Por
    exemplo, saber seu nome nos permite dar as boas-vindas a você na próxima
    vez que visitar o site da MediLaudo. Saber seu país e idioma, se você for
    um cliente, usuário, pessoa interessada em assuntos de saúde ou
    profissional da área de saúde isso pode nos ajudar a fornecer uma
    experiência personalizada e mais útil. Saber se alguém que está usando seu
    computador ou dispositivo comprou um determinado produto ou usou um serviço
    em particular nos ajuda a tornar nossas comunicações de propaganda e por
    e-mail mais relevantes para seus interesses.
</p>
<p>
  Saber suas informações de contato e sobre seu computador ou dispositivo nos
    ajuda a personalizar e otimizar nossos produtos, serviços e site ao seu
    sistema operacional e navegador, além de nos auxiliar no fornecimento de um
    atendimento ao cliente melhor e mais personalizado.
</p>
<p>
  Quando você usa um navegador da Web para acessar os Serviços, pode
    configurá-lo para aceitar todos os cookies, rejeitar todos os cookies ou
    notificar você quando um cookie for recebido. Os navegadores são
    diferentes, por isso, confira o menu “Ajuda” de seu navegador para saber
    como alterar suas preferências relacionadas a cookies. O sistema
    operacional de seu dispositivo pode conter controles adicionais
    relacionados a cookies.
</p>
<p>
  No entanto, alguns Serviços podem ser projetados para funcionar usando
    cookies e, portanto, a desativação dos cookies pode afetar sua capacidade
    de usar esses Serviços ou algumas das partes deles.
</p>
<p>
  Assim como ocorre na maioria dos sites, algumas informações são coletadas
    automaticamente e armazenadas em arquivos de log. Essas informações
    incluem, dentre outras, endereços do Protocolo da Internet (IP), tipo de
    navegador, idioma, provedor de serviços da Internet (ISP), páginas de
    encaminhamento e saída, sistema operacional, marcação de data/hora, ações
    efetuadas pelo usuário, ferramentas e funcionalidades acessadas pelo
    usuário e dados clickstream.
</p>
<p>
  Usamos essas informações para entender e analisar tendências, administrar o
    site, aprender sobre o comportamento e coletar informações demográficas
    sobre nossa base de pacientes como um todo. A MediLaudo pode usar essas
    informações em seus serviços de marketing e propagandas.
</p>
<p>
  Em algumas das mensagens de e-mail, usamos um “URL click-through” vinculado
    ao conteúdo no site da MediLaudo.
</p>
<p>
  Quando os usuários clicam em um desses URLs, eles podem passar por um
    servidor web em separado antes de chegar à página de destino em nosso site.
    Rastreamos esses dados "click-through" para nos ajudar a determinar o
    interesse em tópicos em particular e medir a eficácia de nossas
    comunicações com o usuário. Se preferir não ser rastreado dessa maneira,
    você não deverá clicar no texto ou link gráfico nas mensagens de e-mail.
</p>
<p>
  Pixel tags nos permitem enviar mensagens de e-mail em um formato que os
    usuários podem ler diretamente, sem a necessidade de um servidor externo, o
    que permite que recebamos informações relacionadas à abertura do e-mail e
    tempo de leitura. Podemos utilizar essas informações para reduzir ou
    eliminar mensagens e e-mails enviados aos pacientes.
</p>
<p>
  <strong>Divulgação a terceiros</strong>
</p>
<p>
  A base de dados de informações formada pela MediLaudo pode ser
    disponibilizada a parceiros de negócio estratégicos visando o benefício e
    geração de resultados mútuos, como o fornecimento ou melhora de nossos
    produtos, serviços e propaganda. A base de dados não será compartilhada
    para propósitos de marketing dos parceiros de negócios.
</p>
<p>
  <strong>Outros</strong>
</p>
<p>
    Pode ser necessário − por lei, processo legal, litígio e/ou solicitações de
    autoridades públicas e governamentais dentro ou fora de seu país de
    residência − que a <strong>MediLaudo</strong> revele suas informações
    pessoais. Podemos também revelar informações sobre você se determinarmos
    que, para propósitos de segurança nacional, imposição da lei ou outros
    problemas de importância pública, a revelação será necessária ou
    apropriada.
</p>
<p>
  Também podemos revelar informações sobre você se determinarmos que a
    revelação é razoavelmente necessária para impor nossos termos e condições
    ou proteger nossas operações ou usuários. Além disso, no caso de uma
    reorganização, fusão ou venda, podemos transferir qualquer e todas as
    informações pessoais que coletamos a terceiros relevantes.
</p>
<p>
  <strong>Proteção de dados pessoais</strong>
</p>
<p>
  A <strong>MediLaudo</strong> leva a segurança de seus dados pessoais muito
    a sério. Os serviços on-line da <strong>MediLaudo</strong> protegem suas
    informações pessoais durante a transmissão usando criptografia. Quando seus
    dados pessoais são armazenados pela <strong>MediLaudo</strong>, utilizamos
    sistemas de computador cujo acesso é limitado, uma vez que são mantidos em
    instalações que incluem medidas físicas de segurança. Os dados são
    armazenados de maneira criptografada, mesmo quando utilizados serviços de
    armazenamento de terceiros.
</p>
<p>
Considerando que nenhum sistema de segurança é absolutamente seguro, a    <strong>MediLaudo</strong> se exime de quaisquer responsabilidades por
    eventuais danos e/ou prejuízos decorrentes de falhas, vírus ou invasões do
    banco de dados de seus produtos, serviços e site, salvo nos casos em que
    tiver agido com dolo ou culpa.
</p>
<p>
  Quando algum fórum de mensagem ou serviços de rede social da MediLaudo é
    usado, as informações pessoais que você compartilha são visíveis a outros
    usuários e podem ser lidas, coletadas ou usadas por eles. Você é
    responsável pelas informações pessoais que escolhe enviar nesses casos. Por
    exemplo, se você comenta uma postagem de Facebook, essas informações serão
    públicas.
</p>
<p>
  <strong>Integridade e retenção de dados pessoais</strong>
</p>
<p>
  A MediLaudo permite que você mantenha facilmente suas informações pessoais
    precisas, completas e atualizadas. Reteremos suas informações pessoais pelo
    período necessário para satisfazer as finalidades descritas nesta Política
    de Privacidade, a menos que um período de retenção mais longo seja exigido
    ou permitido por lei.
</p>
<p>
  <strong>Acesso a informações pessoais</strong>
</p>
<p>
  Você pode ajudar a assegurar que suas informações e preferências de contato
    estejam corretas, completas e atualizadas.
</p>
<p>
  No caso de outras informações pessoais que armazenamos, ofereceremos o
    acesso a você para qualquer finalidade, incluindo solicitar a atualização,
    correção e exclusão dos dados, contanto que a MediLaudo não seja obrigada
    por lei ou para fins comerciais legítimos a retê-los. Podemos recusar o
    processamento de solicitações que sejam infundadas/vexatórias, prejudiquem
    a privacidade de outros, sejam extremamente impraticáveis ou cujo acesso
    não seja de outra forma exigido por lei. Seus dados poderão ser
    visualizados no Portal, usando seu login e senha, acessando por site ou
    aplicativo fornecidos por nós.
</p>
<p>
  <strong>Serviços de localização</strong>
</p>
<p>
  Para fornecer serviços de localização em seus produtos e serviços, a
    MediLaudo, seus parceiros e licenciados podem coletar, usar e compartilhar
    dados de localização precisos, incluindo a localização geográfica em tempo
    real de seu computador ou dispositivo da MediLaudo. Esses dados de
    localização são coletados anonimamente de forma a não identificar você
    pessoalmente e são usados pela MediLaudo, seus parceiros e licenciados para
    fornecer e melhorar produtos e serviços de localização. Por exemplo,
    podemos compartilhar sua localização geográfica com provedores de
    aplicativos quando você optar por usar serviços de localização.
</p>
<p>
  <strong>Sites e serviços de terceiros</strong>
</p>
<p>
  Sites, produtos, aplicativos e serviços da <strong>MediLaudo</strong> podem
    conter links para sites, produtos e serviços de terceiros. Nossos produtos
    e serviços podem usar ou oferecer produtos ou serviços de terceiros, por
    exemplo, um aplicativo de terceiros para iOS, Android e/ou Windows Phone.
    Informações coletadas por terceiros, que podem incluir dados de localização
    ou informações para contato, são regidas pelas práticas de privacidade
    desses terceiros, as quais incentivamos você a conhecer.
</p>
<p>
  <strong>
        Nosso compromisso em toda a empresa quanto à sua privacidade
    </strong>
</p>
<p>
  Para buscar a garantia de que suas informações pessoais estão seguras,
    comunicamos nossas diretrizes de privacidade e segurança aos colaboradores
    da <strong>MediLaudo</strong> e impomos estritamente as salvaguardas de
    privacidade na empresa.
</p>
<p>
  <strong>Perguntas de privacidade</strong>
</p>
<p>
Se tiver qualquer dúvida sobre a Política de Privacidade da    <strong>MediLaudo</strong> ou sobre o processamento de dados, ou, ainda, se
    quiser fazer uma reclamação devido a uma possível violação das leis locais
    de privacidade, entre em contato conosco. É possível nos contatar pelo
número de telefone disponível em nosso site,    <strong>www.medilaudo.net</strong>.
</p>
<p>
  Qualquer comunicação dessa natureza será examinada e as respostas, quando
    adequadas, serão enviadas o quanto antes.
</p>
<p>
  A <strong>MediLaudo</strong> pode atualizar sua Política de Privacidade de
    tempos em tempos. Quando alterarmos a política de modo material, será
    postado um aviso em nosso site juntamente com a Política de Privacidade
    atualizada.
</p>
