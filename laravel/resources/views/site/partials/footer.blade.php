<footer>
  <div class="centralizar">

    <div class="colunas">
      <div class="coluna">
        <ul>
          <li>
            <a href="a-medilaudo" title="A medilaudo">A MEDILAUDO</a>
            <ul>
              <li><a href="a-medilaudo" title="Empresa">EMPRESA</a></li>
              <li><a href="a-medilaudo/estrutura" title="Estrutura">ESTRUTURA</a></li>
              <!-- <li><a href="a-medilaudo/equipe" title="Equipe">EQUIPE</a></li> -->
            </ul>
          </li>
        </ul>
      </div>
      <div class="coluna">
        <ul>
          <li>
            <a href="laudos-e-especialidades" title="Laudos & Especialidades">LAUDOS & ESPECIALIDADES</a>
            <ul>
              <li><a href="laudos-e-especialidades" title="Laudos e Atuação">LAUDOS E ATUAÇÃO</a></li>
              <!-- <li><a href="laudos-e-especialidades/metodologia-de-trabalho" title="Metodologia de trabalho">METODOLOGIA DE TRABALHO</a></li> -->
            </ul>
          </li>
        </ul>
      </div>
      <div class="coluna">
        <ul>
          <li><a href="para-hospitais-e-clinicas" title="Para Hospitais e Clínicas">PARA HOSPITAIS E CLÍNICAS</a></li>
          <li><a href="para-medicos" title="Para Médicos">PARA MÉDICOS</a></li>
          <li><a href="suporte" title="Suporte">SUPORTE</a></li>
        </ul>
      </div>
      <div class="coluna">
        <ul>
          <li><a href="noticias" title="Notícias">NOTÍCIAS</a></li>
          <li>
            <a href="faq" title="FAQ">FAQ</a>
            <ul>
              <li><a href="faq" title="Perguntas Frequentes">PERGUNTAS FREQUENTES</a></li>
              <li><a href="#termos-de-uso" id="link-termos-de-uso" title="Termos de Uso">TERMOS DE USO</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="coluna">
        <ul>
          <li><a href="contato" title="Contato">CONTATO</a></li>
          <li>
            <p>
              <span>11 5083-6277</span>
              Rua Borges Lagoa 1083 - 13 andar - Vila Clementino<br>
              04038-420 - Sao Paulo - SP
            </p>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div id="termos-de-uso" class="cke" style="display: none;">
    @include('site.partials.termos-de-uso')
  </div>

  <div class="assinatura">
    &copy; {{date('Y')}} MediLaudo - Todos os direitos reservados | <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa">Criação de Sites: Trupe Agência Criativa</a>
  </div>
</footer>
