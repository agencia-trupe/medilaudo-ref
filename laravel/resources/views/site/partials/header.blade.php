<header>

  <div class="centralizar">

    <a href="{{URL::route('site.index')}}" id="link-home" title="Página Inicial" @if(Route::currentRouteName() != 'site.index') class="internas" @endif>
      <img src="assets/images/layout/logo-medilaudo.png" alt="Medilaudo">
    </a>

    <div id="barra-topo">

      <div class="social">
        @if($contato->linkedin) <a href="{{$contato->linkedin}}" target="_blank" title="LinkedIn"><img src="assets/images/layout/linkedin.png" alt="LinkedIn"></a> @endif
        @if($contato->facebook) <a href="{{$contato->facebook}}" target="_blank" title="Facebook"><img src="assets/images/layout/face.png" alt="Facebook"></a> @endif
      </div>

      <a href="" id="link-login">
        <img src="assets/images/layout/cadeado-login.png" alt="Login"> LOGIN
      </a>

    </div>

    @if(Route::currentRouteName() == 'site.index')
      <div id="frase-header">
        <h1>LAUDOS DE ESPECIALISTAS</h1>
        <h2>A UM CLIQUE DE DISTÂNCIA DA SUA EMPRESA</h2>
        <div class="fundo-skew"></div>
      </div>
    @else
      <div id="imagem-header">
        <img src="assets/images/layout/img-cabec-internas.jpg">
      </div>
    @endif

  </div>

</header>
