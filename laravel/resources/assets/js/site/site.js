$(document).ready(function() {

  $('.tooltip').tooltipster({
    theme: 'tooltipster-light'
  });

  $('#inputArquivo').change( function(){
    val = $(this).val().split('\\');
    $('.cv label').html(val[val.length - 1]);
  });

  $('.lista-faq .questao').click( function(){
    var parent = $(this).parent();
    var target = parent.find('.resposta');

    $('.lista-faq .resposta.aberto').removeClass('aberto');
    target.addClass('aberto');
  });

  $('#toggle-menu').click( function(e){
    e.preventDefault();
    $('nav').toggleClass('aberto');
  });

  $('#link-termos-de-uso').fancybox({
    maxWidth : 1200,
    helpers : {
      title : null
    }
  });

  $('.lista-imagens a').fancybox({
    maxWidth : 1200,
    helpers : {
      title : null
    }
  });

});
