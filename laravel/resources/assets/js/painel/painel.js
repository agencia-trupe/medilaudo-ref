$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})

jQuery(function($){

  $.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c;Anterior',
    nextText: 'Pr&oacute;ximo&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };

  $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function busca(termo){
  $("table").find("tr").each(function(index) {
    if (!index) return;
    var id = $(this).find("td.celula-nome").first().text().toLowerCase();
    $(this).toggle(id.indexOf(termo.toLowerCase()) !== -1);
  });
}

$('document').ready( function(){

  $("input[name=tipo_destaque]").change( function(){
    valor = $(this).val();
    if(valor == 'destaque_video'){
      $('#destaque_video').show();
      $('#destaque_img').hide();
    }else if(valor == 'destaque_imagem'){
      $('#destaque_img').show();
      $('#destaque_video').hide();
    }else if(valor == 'sem_destaque'){
      $('#destaque_img').hide();
      $('#destaque_video').hide();
    }
  });

  $('.toggle-laudo-destaque').on('click', function(){
    var btn = $(this);
    var id = btn.attr('data-id');

    if($(this).hasClass('btn-success'))
      var val = 0;
    else
      var val = 1;

    btn.addClass('carregando');

    $.post('painel/toggle-laudo-destaque', {
      id: id,
      val: val
    }, function(resposta){

      if(val == 1){
        btn.addClass('btn-success').removeClass('btn-danger');
        btn.find('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
      }else{
        btn.removeClass('btn-success').addClass('btn-danger');
        btn.find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
      }

      btn.removeClass('carregando');

    });
  });

  $('.toggle-noticia-destaque').on('click', function(){
    var btn = $(this);
    var id = btn.attr('data-id');

    if($(this).hasClass('btn-success'))
      var val = 0;
    else
      var val = 1;

    btn.addClass('carregando');

    $.post('painel/toggle-noticia-destaque', {
      id: id,
      val: val
    }, function(resposta){

      if(val == 1){
        btn.addClass('btn-success').removeClass('btn-danger');
        btn.find('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
      }else{
        btn.removeClass('btn-success').addClass('btn-danger');
        btn.find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
      }

      btn.removeClass('carregando');

    });
  });

  // Botão de excluir registro
  $('.btn-delete').click( function(e){
  	e.preventDefault();
  	var form = $(this).closest('form');
  	bootbox.confirm('Deseja Excluir o Registro?', function(result){
    	if(result)
      	form.submit();
    	else
      	$(this).modal('hide');
  	});
	});

  // Ordenação na tabela
  $('table.table-sortable tbody').sortable({
    update : function () {
      serial = [];
      tabela = $('table.table-sortable').attr('data-tabela');
      $('table.table-sortable tbody').children('tr.tr-row').each(function(idx, elm) {
        serial.push(elm.id.split('_')[1])
      });
      $.post('painel/gravar-ordem-registros', {
        data : serial,
        tabela : tabela
      });
    },
    helper: function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    handle : $('.btn-move'),
    items : '.tr-row'
  }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

  if($('textarea').length){
    $('textarea').not('.textarea-simples').ckeditor({
      customConfig: '/assets/js/ckeditor_config.js'
    });
  }

});
