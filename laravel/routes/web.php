<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', ['as' => 'site.index', 'uses' => 'Site\Home\HomeController@getIndex']);

Route::get('painel/auth/login', ['as' => 'painel.login','uses' => 'Painel\Auth\LoginController@getLogin']);
Route::post('painel/auth/login', ['as' => 'painel.auth','uses' => 'Painel\Auth\LoginController@postLogin']);
Route::get('painel/auth/logout', ['as' => 'painel.logout','uses' => 'Painel\Auth\LoginController@getLogout']);
Route::get('a-medilaudo', ['as' => 'site.a-medilaudo.index', 'uses' => 'Site\AMedilaudo\AMedilaudoController@getIndex']);
Route::get('a-medilaudo/estrutura', ['as' => 'site.a-medilaudo.estrutura', 'uses' => 'Site\AMedilaudo\AMedilaudoController@getEstrutura']);
Route::get('a-medilaudo/equipe', ['as' => 'site.a-medilaudo.equipe', 'uses' => 'Site\AMedilaudo\AMedilaudoController@getEquipe']);
Route::get('laudos-e-especialidades', ['as' => 'site.laudos-e-especialidades', 'uses' => 'Site\LaudosEEspecialidades\LaudosEEspecialidadesController@getIndex']);
Route::get('para-hospitais-e-clinicas', ['as' => 'site.para-hospitais-e-clinicas', 'uses' => 'Site\ParaHospitaisEClinicas\ParaHospitaisEClinicasController@getIndex']);
Route::post('para-hospitais-e-clinicas', ['as' => 'site.para-hospitais-e-clinicas.enviar', 'uses' => 'Site\ParaHospitaisEClinicas\ParaHospitaisEClinicasController@postEnviar']);
Route::get('para-medicos', ['as' => 'site.para-medicos', 'uses' => 'Site\ParaMedicos\ParaMedicosController@getIndex']);
Route::post('para-medicos', ['as' => 'site.para-medicos.enviar', 'uses' => 'Site\ParaMedicos\ParaMedicosController@postEnviar']);
Route::get('suporte/{slug_categoria?}', ['as' => 'site.suporte', 'uses' => 'Site\Suporte\SuporteController@getIndex']);
Route::get('suporte/download/{id}', ['as' => 'site.suporte', 'uses' => 'Site\Suporte\SuporteController@getDownload']);
Route::get('noticias', ['as' => 'site.noticias', 'uses' => 'Site\Noticias\NoticiasController@getIndex']);
Route::get('noticias/ler/{slug_noticia}', ['as' => 'site.noticias.detalhes', 'uses' => 'Site\Noticias\NoticiasController@getDetalhes']);
Route::get('faq', ['as' => 'site.faq', 'uses' => 'Site\Faq\FaqController@getIndex']);
Route::get('contato', ['as' => 'site.contato', 'uses' => 'Site\Contato\ContatoController@getIndex']);
Route::post('contato', ['as' => 'site.contato.enviar', 'uses' => 'Site\Contato\ContatoController@postEnviar']);
// NOVAS ROTAS SITE

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Painel',
  'prefix' => 'painel'
], function() {

  Route::get('', ['as' => 'painel.dashboard', 'uses' => function() {
    return view('painel.dashboard.index');
  }]);

  Route::get('download/{arquivo}', function(Illuminate\Http\Request $request){
    $arquivo = $request->arquivo;
    return response()->download(base_path().'/resources/assets/curriculos/'.$arquivo);
  });

  Route::post('gravar-ordem-registros', function(Illuminate\Http\Request $request){
    $itens = $request->input('data');
    $tabela = $request->input('tabela');
    for ($i = 0; $i < count($itens); $i++)
      DB::table($tabela)->where('id', $itens[$i])->update(array('ordem' => $i));
  });

  Route::resource('usuarios', 'Usuarios\UsuariosController', ['as' => 'painel']);

  Route::post('imagens/upload', 'Imagens\ImagensController@postUpload');

  Route::post('toggle-laudo-destaque', function(Illuminate\Http\Request  $request){
		$laudo = Medilaudo\Models\Laudo::find($request->id);
		$laudo->destaque = $request->val;
		$laudo->save();
	});

  Route::post('toggle-noticia-destaque', function(Illuminate\Http\Request  $request){
		$noticia = Medilaudo\Models\Noticia::find($request->id);
		$noticia->is_destaque = $request->val;
		$noticia->save();
	});

  // NOVAS ROTAS DO PAINEL:
  Route::resource('equipe', 'Equipe\EquipeController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('contato', 'Contato\ContatoController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
  Route::resource('contatos-recebidos', 'Contato\ContatoRecebidoController', ['only' => ['index', 'show', 'destroy'], 'as' => 'painel']);
  Route::resource('faq', 'Faq\FaqController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('noticias', 'Noticias\NoticiasController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('suporte_arquivos', 'SuporteArquivos\SuporteArquivosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('suporte_categorias', 'SuporteCategorias\SuporteCategoriasController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('motivos_medicos', 'MotivosMedicos\MotivosMedicosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('motivos_hospitais', 'MotivosHospitais\MotivosHospitaisController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('vantagens', 'Vantagens\VantagensController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
  Route::resource('estrutura_topicos', 'EstruturaTopicos\EstruturaTopicosController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
  Route::resource('estrutura', 'Estrutura\EstruturaController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
  Route::resource('empresa', 'Empresa\EmpresaController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
  Route::resource('especialistas', 'Especialistas\EspecialistasController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('laudos', 'Laudos\LaudosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('bannerextra', 'Bannerextra\BannerextraController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
  Route::resource('chamadas', 'Chamadas\ChamadasController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
  Route::resource('videohome', 'VideoHome\VideoHomeController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);

});
