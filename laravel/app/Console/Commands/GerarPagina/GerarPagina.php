<?php

namespace Medilaudo\Console\Commands\GerarPagina;

use Illuminate\Console\Command;

class GerarPagina extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:pagina
                            {resource : The title of the resource}
                            {--appname= : Name of the App}
                            {--route= : Name of the route to bind with the resource}';

    protected $description = 'Create a new Site Resource';

    private $resourcename;
    private $controllername;
    private $route;
    private $modelname;
    private $appname;
    private $dirname;
    private $type;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
      $this->resourcename = $this->argument('resource');
      $this->appname = $this->option('appname');
      $this->route = $this->option('route');

      if(!$this->appname)
        $this->appname = $this->ask('Qual o nome do Aplicativo?');

      if(!$this->route)
        $this->route = $this->ask('Qual a rota da Página?');

      $this->controllername = ucfirst(camel_case($this->route))."Controller";
      $this->dirname = ucfirst(camel_case($this->route));

      // criar Rota
      $this->criarRota();

      // criar Controller
      $this->criarController();

      // criar Views
      $this->criarViews();

    }

    private function criarRota()
    {
      $route_str = "Route::get('".$this->route."', ['as' => 'site.".$this->route."', 'uses' => 'Site\\".$this->dirname."\\".$this->controllername."@getIndex']);\n// NOVAS ROTAS SITE";

      $rotasfile = file_get_contents(app_path().'/../routes/web.php');
      $novorotasfile = str_replace("// NOVAS ROTAS SITE", $route_str, $rotasfile);
      file_put_contents(app_path()."/../routes/web.php", $novorotasfile);
      echo "Rota criada\n";
    }

    private function criarController()
    {
      $conteudo = $this->template('controller');
      $dir = ucfirst(camel_case($this->route));

      $search = array(
        '#APPNAME#',
        '#CONTROLLERNAME#',
        '#DIRNAME#',
        '#ROUTENAMEMIN#'
  		);
  		$replace = array(
        $this->appname,
        $this->controllername,
        $dir,
        strtolower($dir)
  		);

  		$controller = str_replace($search, $replace, $conteudo);

      mkdir(app_path().'/Http/Controllers/Site/'.$dir);
  		file_put_contents(app_path().'/Http/Controllers/Site/'.$dir.'/'.$this->controllername.'.php', $controller);

      echo "Controller criado\n";
    }

    private function criarViews()
    {
      mkdir(app_path().'/../resources/views/site/'.$this->route);
      $this->criarView('index', 'index');
      echo "View criadas\n";
    }

    private function criarView($template, $viewname)
    {
      $conteudo = $this->template($template);
      file_put_contents(app_path().'/../resources/views/site/'.$this->route.'/'.$viewname.'.blade.php', $conteudo);
    }

    private function template($tipo)
	  {
		  return file_get_contents(app_path().'/Console/Commands/GerarPagina/templates/'.$tipo.'.txt');
		}


}
