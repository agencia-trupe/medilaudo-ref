<?php

namespace Medilaudo\Console\Commands\GerarResourceAdmin;

use Illuminate\Console\Command;

class GerarResourceAdmin extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:resource
                            {resource : The title of the resource}
                            {--appname= : Name of the App}
                            {--table= : Name of the table where the resource is stored}
                            {--route= : Name of the route to bind with the resource}
                            {--type=  : The type of the resource}
                            {--model= : The name of the resource model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Admin Resource';

    private $resourcename;
    private $controllername;
    private $tablename;
    private $routename;
    private $modelname;
    private $appname;
    private $criarDir;

    private $indextitle;
    private $entityname;

    private $type;

    private $has_ordenacao;

    private $types_available = [
      'crud_padrao',
      'crud_registro_unico'
    ];


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->resourcename = $this->argument('resource');
      $this->appname = $this->option('appname');
      $this->tablename = $this->option('table');
      $this->routename = $this->option('route');
      $this->type = $this->option('type');
      $this->modelname = $this->option('model');

      if(!$this->appname)
        $this->appname = $this->ask('Qual o nome do Aplicativo?');

      if(!$this->tablename)
        $this->tablename = $this->ask('Qual o nome da tabela em que o recurso está armazenado?');

      if(!$this->routename)
        $this->routename = $this->ask('Qual o nome da rota em que o recurso responde?');

      if(!$this->type || !in_array($this->type, $this->types_available))
        $this->type = $this->choice('Qual o tipo do recurso', ['crud_padrao', 'crud_registro_unico'], 0);

      if(!$this->modelname)
        $this->modelname = $this->ask('Qual o nome do model do recurso?');

      $this->indextitle = $this->ask('Qual o título a ser mostrado na Página do Painel?');
      $this->entityname = ($this->type == 'crud_padrao') ? $this->ask('Qual o título a ser mostrado na botão de Adicionar Registro?') : '';

      $this->criarDir = $this->choice('Criar diretório de imagens?', ['Sim', 'Não'], 0);

      $this->controllername = ucfirst(camel_case($this->routename))."Controller";

      $this->has_ordenacao = \Schema::hasColumn($this->tablename, 'ordem');

      // criar rota
      $this->criarRota();

      // criar model
      $this->criarModel();

      // criar model
      $this->criarController();

      // criar views
      $this->criarViews();

      // criar item no menu
      $this->criarItemMenu();

      if($this->criarDir == 'Sim')
        $this->criarDiretorios();

    }

    private function criarDiretorios(){

      $dir = app_path().'/../../public/assets/images/'.$this->routename;
      $dir_thumbs = $dir.'/thumbs';

      mkdir($dir);
      mkdir($dir_thumbs);
      echo "execute: sudo chmod 0777 -R ../public/assets/images/{$this->routename}\n";
      // chmod($dir, 777);
      // chmod($dir_thumbs, 777);
    }


    private function criarRota()
    {
      if($this->type == 'crud_padrao')
        $route_str = "// NOVAS ROTAS DO PAINEL:\nRoute::resource('".$this->routename."', '".ucfirst(camel_case($this->routename))."\\".$this->controllername."', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);";

      elseif($this->type == 'crud_registro_unico')
        $route_str = "// NOVAS ROTAS DO PAINEL:\nRoute::resource('".$this->routename."', '".ucfirst(camel_case($this->routename))."\\".$this->controllername."', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);";

      $rotasfile = file_get_contents(app_path().'/../routes/web.php');
      $novorotasfile = str_replace("// NOVAS ROTAS DO PAINEL:", $route_str, $rotasfile);
      file_put_contents(app_path()."/../routes/web.php", $novorotasfile);
      echo "Rota criada\n";
    }

    private function criarModel()
    {
      $conteudo = $this->template('model');

  		$search = array(
        '#APPNAME#',
  			'#MODELNAME#',
  			'#TABLENAME#'
  		);
  		$replace = array(
        $this->appname.'\\',
  			$this->modelname,
  			$this->tablename
  		);

  		$model = str_replace($search, $replace, $conteudo);
  		file_put_contents(app_path().'/Models/'.$this->modelname.'.php', $model);

      echo "Model criado\n";
    }

    private function criarController()
    {
      $conteudo = $this->template('controller_'.$this->type);

      $search = array(
        '#APPNAME#',
        '#DIRNAME#',
        '#CONTROLLERNAME#',
        '#MODELNAME#',
        '#GETMETHOD#',
        '#ROUTENAME#',
        '#IMGDIR#'
  		);
  		$replace = array(
        $this->appname.'\\',
        ucfirst(camel_case($this->routename)),
        $this->controllername,
  			$this->modelname,
  			$this->has_ordenacao ? 'ordenado()->get()' : 'all()',
        $this->routename,
        $this->criarDir ? "protected \$pathImagens = '".$this->routename."/';" : ""
  		);

  		$controller = str_replace($search, $replace, $conteudo);
      $dir = ucfirst(camel_case($this->routename));
      mkdir(app_path().'/Http/Controllers/Painel/'.$dir);
  		file_put_contents(app_path().'/Http/Controllers/Painel/'.$dir.'/'.$this->controllername.'.php', $controller);

      echo "Controller criado\n";
    }

    private function criarViews()
    {
      mkdir(app_path().'/../resources/views/painel/'.$this->routename);
      $this->criarView('view_index_'.$this->type, 'index');
      $this->criarView('view_edit', 'edit');

      if($this->type == 'crud_padrao')
        $this->criarView('view_create', 'create');

      echo "Views criadas\n";
    }

    private function criarView($template, $viewname)
    {
      $conteudo = $this->template($template);

      $search = array(
        '#INDEXTITLE#',
        '#ROUTENAME#',
        '#ENTITYNAME#',
        '#TABLESORTABLE#',
        '#DATA-TABLEATTR#',
        '#ORDERTH#',
        '#ORDERTD#'
      );
      $replace = array(
        $this->indextitle,
        $this->routename,
        $this->entityname ?: $this->indextitle,
        $this->has_ordenacao ? " table-sortable" : "",
        $this->has_ordenacao ? " data-tabela='".$this->tablename."'" : "",
        $this->has_ordenacao ? "<th>Ordenar</th>" : "",
        $this->has_ordenacao ? "<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>" : ""
      );

      $view = str_replace($search, $replace, $conteudo);
      file_put_contents(app_path().'/../resources/views/painel/'.$this->routename.'/'.$viewname.'.blade.php', $view);
    }

    private function template($tipo)
	  {
		  return file_get_contents(app_path().'/Console/Commands/GerarResourceAdmin/templates/'.$tipo.'.txt');
		}

    private function criarItemMenu()
    {
      $itemmenu = "<!--RESOURCEMENU-->\n\n<li @if(str_is('painel.".$this->routename."*', Route::currentRouteName())) class='active' @endif><a href='painel/".$this->routename."' title='".$this->indextitle."'>".$this->indextitle."</a></li>";

      $menufile = file_get_contents(app_path().'/../resources/views/painel/partials/menu.blade.php');

      $novomenufile = str_replace("<!--RESOURCEMENU-->", $itemmenu, $menufile);

      file_put_contents(app_path().'/../resources/views/painel/partials/menu.blade.php', $novomenufile);
    }

}
