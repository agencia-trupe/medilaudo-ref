<?php

namespace #APPNAME#Http\Controllers\Painel\#DIRNAME#;

use Illuminate\Http\Request;
use #APPNAME#Http\Controllers\Controller;

use #APPNAME#Models\#MODELNAME#;
use #APPNAME#Libs\Thumbs;

class #CONTROLLERNAME# extends Controller
{
    #IMGDIR#

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = #MODELNAME#::#GETMETHOD#;

      return view('painel.#ROUTENAME#.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.#ROUTENAME#.edit')->with('registro', #MODELNAME#::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = #MODELNAME#::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.#ROUTENAME#.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
