<?php

namespace Medilaudo;

use Illuminate\Foundation\Application;

class MedilaudoApp extends Application
{
    public function publicPath()
    {
        return $this->basePath.'/../public_html';
        //return '/home/bruno/www/medilaudo/public/';
    }
}
?>
