<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class ContatoMedico extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'contatos_especialistas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nome',
    'email',
    'telefone',
    'especialidade',
    'titulo_cbr',
    'mensagem',
    'curriculo',
    'created_at'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }
}
