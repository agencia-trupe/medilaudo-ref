<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'equipe';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function getCategoriaFormatadaAttribute()
  {
    return ($this->categoria == 'equipe_medica') ? 'Equipe Médica' : 'Equipe Técnica';
  }

  public function scopeTecnica($query)
  {
    return $query->where('categoria', 'equipe_tecnica');
  }

  public function scopeMedica($query)
  {
    return $query->where('categoria', 'equipe_medica');
  }
}
