<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class Laudo extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'laudos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function scopeDestaque($query)
  {
    return $query->where('destaque', '=', 1);
  }

  public function especialistas()
  {
    return $this->hasMany('Medilaudo\Models\Especialista', 'laudos_id')->orderBy('nome');
  }
}
