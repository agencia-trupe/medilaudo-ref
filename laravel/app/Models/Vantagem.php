<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class Vantagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'laudos_vantagens';  

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }
}
