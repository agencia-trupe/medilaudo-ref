<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class Especialista extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'especialistas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function area()
  {
    return $this->belongsTo('Medilaudo\Models\Laudo', 'laudos_id');
  }
}
