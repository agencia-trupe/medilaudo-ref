<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'noticias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  protected $dates = [
    'data',
    'created_at',
    'updated_at'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }

  public function scopeDestaques($query)
  {
    return $query->where('is_destaque', 1);
  }

  public function scopeNaoDestaques($query)
  {
    return $query->where('is_destaque', 0);
  }

  public function setDataAttribute($value)
  {
    list($dia, $mes, $ano) = explode('/', $value);
    $this->attributes['data'] = $ano.'-'.$mes.'-'.$dia;
  }

  public function imagens()
  {
    return $this->hasMany('Medilaudo\Models\NoticiaImagem', 'noticias_id')->orderBy('ordem', 'asc');
  }
}
