<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon as Carbon;

class BannerExtra extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'banners_extras';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  protected $dates = [
    'data_inicio',
    'data_fim',
    'created_at',
    'updated_at'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function setDataInicioAttribute($value)
  {
    list($dia, $mes, $ano) = explode('/', $value);
    $this->attributes['data_inicio'] = $ano.'-'.$mes.'-'.$dia;
  }

  public function setDataFimAttribute($value)
  {
    list($dia, $mes, $ano) = explode('/', $value);
    $this->attributes['data_fim'] = $ano.'-'.$mes.'-'.$dia;
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function scopeAtivos($query)
  {
    $hoje = Carbon::now();
    return $query->where('data_inicio', '<=', $hoje)->where('data_fim', '>=', $hoje);
  }
}
