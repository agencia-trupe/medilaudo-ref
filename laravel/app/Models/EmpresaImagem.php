<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaImagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'empresa_imagens';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'imagem',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }
}
