<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class ContatoRecebido extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'contatos_recebidos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nome',
    'email',
    'telefone',
    'mensagem',
    'created_at'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }
}
