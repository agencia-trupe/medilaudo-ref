<?php

namespace Medilaudo\Models;

use Illuminate\Database\Eloquent\Model;

class SuporteArquivo extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'suporte_arquivos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('titulo', 'asc');
  }

  public function scopeCategoria($query, $id)
  {
    return $query->where('categorias_id', $id);
  }
}
