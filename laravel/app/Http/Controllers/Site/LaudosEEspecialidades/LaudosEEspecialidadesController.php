<?php

namespace Medilaudo\Http\Controllers\Site\LaudosEEspecialidades;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Vantagem;
use Medilaudo\Models\Laudo;

class LaudosEEspecialidadesController extends Controller
{

  public function getIndex(Request $request)
  {
    $laudos = Laudo::ordenado()->get();
    $vantagens = Vantagem::ordenado()->get();
    return view('site.laudos-e-especialidades.index', compact('laudos', 'vantagens'));
  }

}
