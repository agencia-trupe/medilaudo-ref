<?php

namespace Medilaudo\Http\Controllers\Site\Home;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Laudo;
use Medilaudo\Models\Chamada;
use Medilaudo\Models\VideoHome;
use Medilaudo\Models\BannerExtra;

class HomeController extends Controller
{

  public function getIndex(Request $request)
  {
    $videohome = VideoHome::first();
    $chamadas = Chamada::ordenado()->get();
    $listaLaudos = Laudo::destaque()->ordenado()->get();
    $bannerExtra = BannerExtra::ativos()->ordenado()->get();

    return view('site.home.index', [
      'videohome' => $videohome,
      'chamadas' => $chamadas,
      'listaLaudos' => $listaLaudos,
      'bannerExtra' => $bannerExtra
    ]);
  }

}
