<?php

namespace Medilaudo\Http\Controllers\Site\ParaHospitaisEClinicas;

use Mail;
use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\MotivosHospitais;
use Medilaudo\Models\ContatoClinica;

class ParaHospitaisEClinicasController extends Controller
{

  public function getIndex(Request $request)
  {
    $motivos = MotivosHospitais::ordenado()->get();

    return view('site.para-hospitais-e-clinicas.index', compact('motivos'));
  }

  public function postEnviar(Request $request)
  {
    $this->validate($request, [
      'nome' => 'required',
      'email' => 'required|email',
      'hospital' => 'required',
      'telefone' => 'required'
    ]);

    $data['nome'] = $request->nome;
    $data['email'] = $request->email;
    $data['hospital'] = $request->hospital;
    $data['telefone'] = $request->telefone;
    $data['mensagem'] = $request->mensagem;

    Mail::send('emails.contato-hospitais', $data, function($message) use ($data)
    {
      $message->to('comercial@medilaudo.net')
              ->subject('Contato via site - Hospitais e Clínicas - '.$data['nome'])
              ->replyTo($data['email'], $data['nome']);
    });

    $novo = ContatoClinica::create($data);

    $request->session()->flash('contato_enviado', true);

    return redirect( url()->previous() . '#resposta-form');
  }

}
