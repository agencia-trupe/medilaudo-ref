<?php

namespace Medilaudo\Http\Controllers\Site\Noticias;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Noticia;
use Medilaudo\Models\NoticiaImagem;

class NoticiasController extends Controller
{

  public function getIndex(Request $request)
  {
    $destaques = Noticia::destaques()->ordenado()->get();
    $noticias = Noticia::naoDestaques()->ordenado()->get();

    return view('site.noticias.index', compact('noticias', 'destaques'));
  }

  public function getDetalhes(Request $request, $slug_noticia)
  {
    $detalhe = Noticia::where('slug', $slug_noticia)->first();

    if(!$detalhe) return redirect()->to('noticias');

    $noticias = Noticia::destaques()->where('id', '!=', $detalhe->id)->ordenado()->get();

    $anterior = Noticia::ordenado()->where('data', '<', $detalhe->data)->first();
    $proximo = Noticia::orderBy('data', 'asc')->where('data', '>', $detalhe->data)->first();

    $imagens = NoticiaImagem::where('noticias_id', $detalhe->id)->ordenado()->get();

    return view('site.noticias.detalhes', compact('detalhe', 'imagens', 'noticias', 'anterior', 'proximo'));
  }

}
