<?php

namespace Medilaudo\Http\Controllers\Site\AMedilaudo;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Equipe;
use Medilaudo\Models\Empresa;
use Medilaudo\Models\EmpresaImagem;
use Medilaudo\Models\Estrutura;
use Medilaudo\Models\EstruturaImagem;
use Medilaudo\Models\EstruturaTopico;

class AMedilaudoController extends Controller
{

  public function getIndex(Request $request)
  {
    $texto = Empresa::first();
    $imagens = EmpresaImagem::ordenado()->get();

    return view('site.a-medilaudo.index', compact('texto', 'imagens'));
  }

  public function getEstrutura(Request $request)
  {
    $texto = Estrutura::first();
    $imagens = EstruturaImagem::ordenado()->get();
    $topicos = EstruturaTopico::get();

    return view('site.a-medilaudo.estrutura', compact('texto', 'imagens', 'topicos'));
  }

  public function getEquipe(Request $request)
  {
    $equipe_medica = Equipe::medica()->ordenado()->get();
    $equipe_tecnica = Equipe::tecnica()->ordenado()->get();
    return view('site.a-medilaudo.equipe', compact('equipe_medica', 'equipe_tecnica'));
  }

}
