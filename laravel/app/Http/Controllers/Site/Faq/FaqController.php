<?php

namespace Medilaudo\Http\Controllers\Site\Faq;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Faq;

class FaqController extends Controller
{

  public function getIndex(Request $request)
  {
    $faq = Faq::ordenado()->get();

    return view('site.faq.index', compact('faq'));
  }

}
