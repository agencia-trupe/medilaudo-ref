<?php

namespace Medilaudo\Http\Controllers\Site\Suporte;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\SuporteArquivo;
use Medilaudo\Models\SuporteCategoria;

class SuporteController extends Controller
{

  public function getIndex(Request $request, $slug_categoria = null)
  {
    if($slug_categoria){

      $cat = SuporteCategoria::where('slug', $slug_categoria)->first();

      if($cat)
        $arquivos = SuporteArquivo::categoria($cat->id)->ordenado()->get();
      else
        return redirect()->route('site.suporte');
    }
    else
      $arquivos = SuporteArquivo::ordenado()->get();

    $categorias = SuporteCategoria::ordenado()->get();

    return view('site.suporte.index', compact('arquivos', 'categorias', 'slug_categoria'));
  }

  public function getDownload(Request $request, $id_arquivo)
  {
    $arquivo = SuporteArquivo::find($id_arquivo);

    if(!$arquivo) return redirect()->back();

    $xpd = explode('.', $arquivo->arquivo);
    $extensao = end($xpd);

    return response()->download('assets/arquivos/'.$arquivo->arquivo, str_slug($arquivo->titulo).'.'.$extensao);
  }
}
