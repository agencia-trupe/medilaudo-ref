<?php

namespace Medilaudo\Http\Controllers\Site\Contato;

use Mail;
use Medilaudo\Models\ContatoRecebido;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

class ContatoController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.contato.index');
  }

  public function postEnviar(Request $request)
  {
    $this->validate($request, [
      'nome' => 'required',
      'email' => 'required|email',
      'mensagem' => 'required'
    ]);

    $data['nome'] = $request->nome;
    $data['email'] = $request->email;
    $data['telefone'] = $request->telefone;
    $data['mensagem'] = $request->mensagem;

    Mail::send('emails.contato', $data, function($message) use ($data)
    {
      $message->to('comercial@medilaudo.net')
              ->subject('Contato via site - '.$data['nome'])
              ->replyTo($data['email'], $data['nome']);
    });

    $novo = ContatoRecebido::create($data);

    $request->session()->flash('contato_enviado', true);

    return redirect()->back();
  }
}
