<?php

namespace Medilaudo\Http\Controllers\Site\ParaMedicos;

use Mail;
use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\MotivosMedicos;
use Medilaudo\Models\ContatoMedico;

class ParaMedicosController extends Controller
{

  public function getIndex(Request $request)
  {
    $motivos = MotivosMedicos::ordenado()->get();

    return view('site.para-medicos.index', compact('motivos'));
  }

  public function postEnviar(Request $request)
  {
    $this->validate($request, [
      'nome' => 'required',
      'email' => 'required|email',
      'telefone' => 'required',
      'especialidade' => 'required',
      'titulo_cbr' => 'required',
      'arquivo' => 'required|mimes:pdf'
    ]);

    $data['nome'] = $request->nome;
    $data['email'] = $request->email;
    $data['telefone'] = $request->telefone;
    $data['especialidade'] = $request->especialidade;
    $data['titulo_cbr'] = $request->titulo_cbr;
    $data['mensagem'] = $request->mensagem;

    $arquivo = $request->arquivo;
    list($nomearquivo,$extensao) = explode('.', $arquivo->getClientOriginalName());
    $filename = str_slug($nomearquivo).'.'.$extensao;
    $arquivo->move(base_path('resources/assets/curriculos/'), $filename);
    $data['curriculo'] = $filename;

    Mail::send('emails.contato-especialistas', $data, function($message) use ($data)
    {
      $message->to('atendimento@medilaudo.net')
              ->subject('Contato via site - Cadastro de Profissional - '.$data['nome'])
              ->replyTo($data['email'], $data['nome'])
              ->attach(base_path('resources/assets/curriculos/'.$data['curriculo']));
    });

    $novo = ContatoMedico::create($data);

    $request->session()->flash('contato_enviado', true);

    return redirect( url()->previous() . '#resposta-form');
  }
}
