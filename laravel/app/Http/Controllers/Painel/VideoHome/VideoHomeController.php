<?php

namespace Medilaudo\Http\Controllers\Painel\VideoHome;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\VideoHome;
use Medilaudo\Libs\Thumbs;

class VideoHomeController extends Controller
{
    protected $pathImagens = 'video_home/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = VideoHome::all();

      return view('painel.video_home.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.video_home.edit')->with('registro', VideoHome::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'link_video' => 'required'
    	]);

      $object = VideoHome::find($id);

      $object->link_video = $request->link_video;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.videohome.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
