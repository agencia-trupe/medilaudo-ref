<?php

namespace Medilaudo\Http\Controllers\Painel\Noticias;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Noticia;
use Medilaudo\Models\NoticiaImagem;
use Medilaudo\Libs\Thumbs;
use Medilaudo\Libs\Youtube as Youtube;

class NoticiasController extends Controller
{
    protected $pathImagens = 'noticias/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Noticia::ordenado()->get();

      return view('painel.noticias.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.noticias.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:noticias,titulo',
        'data' => 'required|date_format:d/m/Y'
    	]);

      $object = new Noticia;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->chamada = $request->chamada;
      $object->data = $request->data;
      $object->texto = $request->texto;

      $object->tipo_destaque = $request->tipo_destaque;

      if($object->tipo_destaque == 'destaque_video'){

        $object->destaque_imagem = '';
        $object->destaque_video = $request->destaque_link_video;

        $video_object = new Youtube($request->destaque_link_video);
        $object->thumb_video = $video_object->getThumbnail();

      }elseif($object->tipo_destaque == 'destaque_imagem'){

        $imagem = Thumbs::make($request, 'destaque_imagem', 400, null, $this->pathImagens);
        if($imagem){
        	Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/');
        	$object->destaque_imagem = $imagem;
        }
        $object->destaque_video = '';
        $object->thumb_video = '';

      }elseif($object->tipo_destaque == 'sem_destaque'){

        $object->destaque_imagem = '';
        $object->destaque_video = '';
        $object->thumb_video = '';

      }

      try {

        $object->save();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new NoticiaImagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.noticias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.noticias.edit')->with('registro', Noticia::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo' => 'required|unique:noticias,titulo,'.$id,
        'data' => 'required|date_format:d/m/Y'
    	]);

      $object = Noticia::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->chamada = $request->chamada;
      $object->data = $request->data;
      $object->texto = $request->texto;

      $object->tipo_destaque = $request->tipo_destaque;

      if($object->tipo_destaque == 'destaque_video'){

        $object->destaque_imagem = '';
        $object->destaque_video = $request->destaque_link_video;

        $video_object = new Youtube($request->destaque_link_video);
        $object->thumb_video = $video_object->getThumbnail();

      }elseif($object->tipo_destaque == 'destaque_imagem'){

        $imagem = Thumbs::make($request, 'destaque_imagem', 400, null, $this->pathImagens);
        if($imagem){
        	Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/');
        	$object->destaque_imagem = $imagem;
        }
        $object->destaque_video = '';
        $object->thumb_video = '';

      }elseif($object->tipo_destaque == 'sem_destaque'){

        $object->destaque_imagem = '';
        $object->destaque_video = '';
        $object->thumb_video = '';

      }

      try {

        $object->save();

        $imgs_atuais = $object->imagens;
        foreach ($imgs_atuais as $img_atual)
          $img_atual->delete();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new NoticiaImagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.noticias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Noticia::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.noticias.index');
    }

}
