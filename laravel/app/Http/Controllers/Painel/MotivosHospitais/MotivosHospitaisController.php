<?php

namespace Medilaudo\Http\Controllers\Painel\MotivosHospitais;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\MotivosHospitais;
use Medilaudo\Libs\Thumbs;

class MotivosHospitaisController extends Controller
{
    protected $pathImagens = 'motivos_hospitais/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = MotivosHospitais::ordenado()->get();

      return view('painel.motivos_hospitais.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.motivos_hospitais.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'texto' => 'required'
    	]);

      $object = new MotivosHospitais;

      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.motivos_hospitais.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.motivos_hospitais.edit')->with('registro', MotivosHospitais::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'texto' => 'required'
    	]);

      $object = MotivosHospitais::find($id);

      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.motivos_hospitais.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = MotivosHospitais::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.motivos_hospitais.index');
    }

}
