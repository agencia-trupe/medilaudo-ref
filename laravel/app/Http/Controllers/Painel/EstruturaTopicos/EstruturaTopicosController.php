<?php

namespace Medilaudo\Http\Controllers\Painel\EstruturaTopicos;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\EstruturaTopico;
use Medilaudo\Libs\Thumbs;

class EstruturaTopicosController extends Controller
{
    protected $pathImagens = 'estrutura_topicos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = EstruturaTopico::all();

      return view('painel.estrutura_topicos.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.estrutura_topicos.edit')->with('registro', EstruturaTopico::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $object = EstruturaTopico::find($id);

      $object->texto = $request->texto;

      $imagem_1 = Thumbs::make($request, 'imagem_1', 400, null, $this->pathImagens);
      $remover_imagem_1 = $request->has('remover_imagem_1') && $request->remover_imagem_1 == 1 ? true : false;
      if($imagem_1){
      	Thumbs::make($request, 'imagem_1', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem_1 = $imagem_1;
      }
      if($remover_imagem_1)
        $object->imagem_1 = '';

      $imagem_2 = Thumbs::make($request, 'imagem_2', 400, null, $this->pathImagens);
      $remover_imagem_2 = $request->has('remover_imagem_2') && $request->remover_imagem_2 == 1 ? true : false;
      if($imagem_2){
      	Thumbs::make($request, 'imagem_2', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem_2 = $imagem_2;
      }
      if($remover_imagem_2)
        $object->imagem_2 = '';

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.estrutura_topicos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
