<?php

namespace Medilaudo\Http\Controllers\Painel\Laudos;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Laudo;
use Medilaudo\Libs\Thumbs;

class LaudosController extends Controller
{
    protected $pathImagens = 'laudos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Laudo::ordenado()->get();

      return view('painel.laudos.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.laudos.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:laudos,titulo'
    	]);

      $object = new Laudo;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);      

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.laudos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.laudos.edit')->with('registro', Laudo::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:laudos,titulo,'.$id
    	]);

      $object = Laudo::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);      

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.laudos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Laudo::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.laudos.index');
    }

}
