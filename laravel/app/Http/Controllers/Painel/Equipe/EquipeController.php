<?php

namespace Medilaudo\Http\Controllers\Painel\Equipe;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Equipe;
use Medilaudo\Libs\Thumbs;

class EquipeController extends Controller
{
    protected $pathImagens = 'equipe/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Equipe::ordenado()->get();

      return view('painel.equipe.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.equipe.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'area' => 'required',
        'nome' => 'required',
        'imagem' => 'required|image'
    	]);

      $object = new Equipe;

      $object->area = $request->area;
      $object->nome = $request->nome;
      $object->categoria = $request->categoria;
      $object->cargo = $request->cargo;
      $object->descritivo = $request->descritivo;

      $imagem = Thumbs::make($request, 'imagem', 300, 200, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.equipe.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.equipe.edit')->with('registro', Equipe::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'area' => 'required',
        'nome' => 'required',
        'imagem' => 'sometimes|image'
    	]);

      $object = Equipe::find($id);

      $object->area = $request->area;
      $object->nome = $request->nome;
      $object->categoria = $request->categoria;
      $object->cargo = $request->cargo;
      $object->descritivo = $request->descritivo;

      $imagem = Thumbs::make($request, 'imagem', 300, 200, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.equipe.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Equipe::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.equipe.index');
    }

}
