<?php

namespace Medilaudo\Http\Controllers\Painel\Chamadas;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Chamada;
use Medilaudo\Libs\Thumbs;

class ChamadasController extends Controller
{
    protected $pathImagens = 'chamadas/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Chamada::ordenado()->get();

      return view('painel.chamadas.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.chamadas.edit')->with('registro', Chamada::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'texto' => 'required',
        'link' => 'required'
    	]);

      $object = Chamada::find($id);

      $object->link = $request->link;
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.chamadas.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
