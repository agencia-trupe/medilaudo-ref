<?php

namespace Medilaudo\Http\Controllers\Painel\Contato;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Contato;
use Medilaudo\Libs\Thumbs;

use Medilaudo\Models\ContatoMedico;
use Medilaudo\Models\ContatoClinica;
use Medilaudo\Models\ContatoRecebido;

class ContatoRecebidoController extends Controller
{

  public function index(Request $request)
  {
    $filtro = $request->filtro;

    if($filtro == 'medicos'){

      $registros = ContatoMedico::ordenado()->get();
      $filtro_extenso = 'Médicos';

    }elseif($filtro == 'clinicas'){

      $registros = ContatoClinica::ordenado()->get();
      $filtro_extenso = 'Hospitais e Clínicas';

    }elseif($filtro == 'contatos' || !$filtro){

      $registros = ContatoRecebido::ordenado()->get();
      $filtro_extenso = 'Contatos';
      $filtro = 'contatos';

    }

    return view('painel.contato.recebidos')->with(compact('registros', 'filtro', 'filtro_extenso'));
  }

  public function show(Request $request, $id)
  {
    $filtro = $request->filtro;

    if(!$filtro) return redirect()->back();

    if($filtro == 'medicos'){

      $registro = ContatoMedico::find($id);
      $filtro_extenso = 'Médicos';

    }elseif($filtro == 'clinicas'){

      $registro = ContatoClinica::find($id);
      $filtro_extenso = 'Hospitais e Clínicas';

    }elseif($filtro == 'contatos'){

      $registro = ContatoRecebido::find($id);
      $filtro_extenso = 'Contatos';

    }

    return view('painel.contato.recebidos-detalhes')->with(compact('registro', 'filtro', 'filtro_extenso'));
  }

  public function destroy(Request $request, $id)
  {

    $filtro = $request->filtro;

    if(!$filtro) return redirect()->back();

    if($filtro == 'medicos'){

      $object = ContatoMedico::find($id);

    }elseif($filtro == 'clinicas'){

      $object = ContatoClinica::find($id);

    }elseif($filtro == 'contatos'){

      $object = ContatoRecebido::find($id);

    }
    $object->delete();

    $request->session()->flash('sucesso', 'Registro removido com sucesso.');

    return redirect()->route('painel.contatos-recebidos.index', ['filtro' => $filtro]);
  }

}
