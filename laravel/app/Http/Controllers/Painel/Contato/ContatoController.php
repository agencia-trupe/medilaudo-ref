<?php

namespace Medilaudo\Http\Controllers\Painel\Contato;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Contato;
use Medilaudo\Libs\Thumbs;

class ContatoController extends Controller
{
    protected $pathImagens = 'contato/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Contato::all();

      return view('painel.contato.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.contato.edit')->with('registro', Contato::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $object = Contato::find($id);

      $object->telefone = $request->telefone;
      $object->endereco = $request->endereco;
      $object->google_maps = $request->google_maps;
      $object->linkedin = $request->linkedin;
      $object->facebook = $request->facebook;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.contato.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
