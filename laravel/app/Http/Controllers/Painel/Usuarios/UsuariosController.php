<?php

namespace Medilaudo\Http\Controllers\Painel\Usuarios;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Hash;

use Medilaudo\Models\Usuario;

class UsuariosController extends Controller{

	public function index()
	{
		$usuarios = Usuario::all();

		return view('painel.usuarios.index')->with(compact('usuarios'));
	}

	public function create()
	{
		return view('painel.usuarios.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
      'nome'             => 'required',
      'login'            => 'required|unique:admin,login',
      'password'         => 'required|min:6',
      'password_confirm' => 'required|min:6|same:password',
    ]);

		$object = new Usuario;

		$object->nome     = $request->nome;
    $object->login    = $request->login;
		$object->email    = $request->email;
		$object->password = Hash::make($request->password);

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Usuário criado com sucesso.');

			return redirect()->route('painel.usuarios.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

		}
	}

	public function edit($id)
	{
		$usuario = Usuario::find($id);

		return view('painel.usuarios.edit')->with(compact('usuario'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
      'nome'             => 'required',
      'login'            => 'required|unique:admin,login,'.$id,
      'password'         => 'sometimes|min:6',
      'password_confirm' => 'required_with:password|min:6|same:password',
    ]);

		$object = Usuario::find($id);

		$object->email = $request->email;
    $object->login = $request->login;
		$object->nome = $request->nome;

		if($request->has('password'))
			$object->password = Hash::make($request->password);

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Usuário alterado com sucesso.');

			return redirect()->route('painel.usuarios.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = Usuario::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Usuário removido com sucesso.');

		return redirect()->route('painel.usuarios.index');
	}
}
