<?php

namespace Medilaudo\Http\Controllers\Painel\Estrutura;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Libs\Thumbs;
use Medilaudo\Models\Estrutura;
use Medilaudo\Models\EstruturaImagem;

class EstruturaController extends Controller
{
    protected $pathImagens = 'estrutura/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Estrutura::all();

      return view('painel.estrutura.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $imagens = EstruturaImagem::ordenado()->get();
      return view('painel.estrutura.edit')->with('registro', Estrutura::find($id))->with(compact('imagens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'subtitulo' => 'required'
    	]);

      $object = Estrutura::find($id);

      $object->subtitulo = $request->subtitulo;
      $object->texto = $request->texto;

      try {

        $object->save();

        $imgs_atuais = EstruturaImagem::all();
        foreach ($imgs_atuais as $img_atual)
          $img_atual->delete();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            EstruturaImagem::create([
              'imagem' => $img,
              'ordem' => $ordem
            ]);
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.estrutura.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
