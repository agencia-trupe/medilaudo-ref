<?php

namespace Medilaudo\Http\Controllers\Painel\Bannerextra;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\BannerExtra;
use Medilaudo\Libs\Thumbs;

class BannerextraController extends Controller
{
    protected $pathImagens = 'bannerextra/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = BannerExtra::ordenado()->get();

      return view('painel.bannerextra.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.bannerextra.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'titulo' => 'required',
        'link' => 'required',
        'data_inicio' => 'required|date_format:d/m/Y',
        'data_termino' => 'required|date_format:d/m/Y'
    	]);

      $object = new BannerExtra;

      $object->titulo = $request->titulo;
      $object->link = $request->link;
      $object->data_inicio = $request->data_inicio;
      $object->data_fim = $request->data_termino;

      $imagem = Thumbs::make($request, 'imagem', 560, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.bannerextra.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.bannerextra.edit')->with('registro', BannerExtra::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required',
        'link' => 'required',
        'data_inicio' => 'required|date_format:d/m/Y',
        'data_termino' => 'required|date_format:d/m/Y'
    	]);

      $object = BannerExtra::find($id);

      $object->titulo = $request->titulo;
      $object->link = $request->link;
      $object->data_inicio = $request->data_inicio;
      $object->data_fim = $request->data_termino;

      $imagem = Thumbs::make($request, 'imagem', 560, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.bannerextra.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = BannerExtra::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.bannerextra.index');
    }

}
