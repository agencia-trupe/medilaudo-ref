<?php

namespace Medilaudo\Http\Controllers\Painel\Imagens;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Libs\Thumbs;

class ImagensController extends Controller
{

  /*
    Armazena as imagens enviadas via Ajax
  */
  public function postUpload(Request $request)
  {
    $arquivo = $request->file('files');
    $path    = $request->input('path');

    $path_original = "assets/images/{$path}/originais/";
    $path_asset    = "assets/images/{$path}/redimensionadas/";
    $path_thumb2   = "assets/images/{$path}/thumbs-quadradas/";
    $path_thumb3   = "assets/images/{$path}/thumbs-retangulares/";
    $path_thumb    = "assets/images/{$path}/thumbs/";
    $path_upload   = public_path($path_original);

    // $this->criarDiretoriosPadrao($path);

    $nome_arquivo = $arquivo->getClientOriginalName();
    $extensao_arquivo = $arquivo->getClientOriginalExtension();
    $nome_arquivo_sem_extensao = str_replace($extensao_arquivo, '', $nome_arquivo);

    $filename = date('YmdHis').str_slug($nome_arquivo_sem_extensao).'.'.$extensao_arquivo;

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $name = $path_original.$filename;
    $thumb = $path_thumb.$filename;
    $thumb_quadrada = $path_thumb2.$filename;

    // Armazenar Redimensionada

    // redimensionadas
    Thumbs::makeFromFile($path_upload, $filename, 1200, null, public_path($path_asset), 'rgba(255,255,255,1)', false);

    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, null, public_path($path_thumb), 'rgba(255,255,255,1)', false);
    Thumbs::makeFromFile($path_upload, $filename, 300, 300, public_path($path_thumb2), 'rgba(255,255,255,1)', true);
    Thumbs::makeFromFile($path_upload, $filename, 800, 532, public_path($path_thumb3), 'rgba(255,255,255,1)', true);

    return [
      'envio' => 1,
      'thumb' => $thumb_quadrada,
      'filename' => $filename
    ];
  }


  private function criarDiretoriosPadrao($path)
  {
    if(!file_exists(public_path("assets/images/{$path}")))
      mkdir(public_path("assets/images/{$path}"), 0777);

    if(!file_exists(public_path("assets/images/{$path}/originais/")))
      mkdir(public_path("assets/images/{$path}/originais/"), 0777);

    if(!file_exists(public_path("assets/images/{$path}/redimensionadas/")))
      mkdir(public_path("assets/images/{$path}/redimensionadas/"), 0777);

    if(!file_exists(public_path("assets/images/{$path}/thumbs/")))
      mkdir(public_path("assets/images/{$path}/thumbs/"), 0777);

    if(!file_exists(public_path("assets/images/{$path}/thumbs-quadradas/")))
      mkdir(public_path("assets/images/{$path}/thumbs-quadradas/"), 0777);

    if(!file_exists(public_path("assets/images/{$path}/thumbs-retangulares/")))
      mkdir(public_path("assets/images/{$path}/thumbs-retangulares/"), 0777);
  }

}
