<?php

namespace Medilaudo\Http\Controllers\Painel\Especialistas;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\Laudo;
use Medilaudo\Models\Especialista;
use Medilaudo\Libs\Thumbs;

class EspecialistasController extends Controller
{
    protected $pathImagens = 'especialistas/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $laudo = Laudo::find($request->laudos_id);

      return view('painel.especialistas.index')->with(compact('laudo'));
    }

    public function create(Request $request)
    {
      $laudo = Laudo::find($request->laudos_id);

      return view('painel.especialistas.create', ['laudo' => $laudo]);
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'laudos_id' => 'required|exists:laudos,id',
      	'nome' => 'required'
    	]);

      $object = new Especialista;

      $object->laudos_id = $request->laudos_id;
      $object->nome = $request->nome;
      $object->crm = $request->crm;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.especialistas.index', ['laudos_id' => $object->laudos_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.especialistas.edit')->with('registro', Especialista::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'nome' => 'required'
    	]);

      $object = Especialista::find($id);

      $object->nome = $request->nome;
      $object->crm = $request->crm;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.especialistas.index', ['laudos_id' => $object->laudos_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Especialista::find($id);
      $id = $object->laudos_id;
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.especialistas.index', ['laudos_id' => $id]);
    }

}
