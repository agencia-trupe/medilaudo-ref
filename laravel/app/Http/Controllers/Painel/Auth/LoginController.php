<?php

namespace Medilaudo\Http\Controllers\Painel\Auth;

use Auth;
use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectPath = '/painel';
    protected $loginPath = '/painel/auth/login';
    protected $redirectAfterLogout = '/painel';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/painel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getLogin()
    {
      return view('painel.login.index');
    }

    public function postLogin(Request $request)
    {
      $this->validate($request, [
        'login' => 'required',
        'password' => 'required',
      ]);

      $credentials = $this->getCredentials($request);

      if (Auth::attempt($credentials, $request->has('remember'))) {
          return redirect()->intended($this->redirectPath());
      }

      return redirect($this->loginPath)
              ->withInput($request->only('login', 'remember'))
              ->withErrors([
                  'login' => $this->getFailedLoginMessage(),
              ]);
    }

    public function username()
    {
      return 'login';
    }

    protected function getFailedLoginMessage()
    {
      return 'Usuário ou senha inválidos.';
    }

    protected function getCredentials(Request $request)
    {
      return $request->only('login', 'password');
    }

    public function getLogout()
    {
      Auth::logout();

      return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
}
