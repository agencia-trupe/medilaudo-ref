<?php

namespace Medilaudo\Http\Controllers\Painel\SuporteCategorias;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\SuporteCategoria;
use Medilaudo\Libs\Thumbs;

class SuporteCategoriasController extends Controller
{
    protected $pathImagens = 'suporte_categorias/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = SuporteCategoria::ordenado()->get();

      return view('painel.suporte_categorias.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.suporte_categorias.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:suporte_categorias,titulo'
    	]);

      $object = new SuporteCategoria;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.suporte_categorias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.suporte_categorias.edit')->with('registro', SuporteCategoria::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:suporte_categorias,titulo,'.$id
    	]);

      $object = SuporteCategoria::find($id);

      $object->slug = str_slug($request->titulo);

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.suporte_categorias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = SuporteCategoria::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.suporte_categorias.index');
    }

}
