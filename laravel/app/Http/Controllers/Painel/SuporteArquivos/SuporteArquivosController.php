<?php

namespace Medilaudo\Http\Controllers\Painel\SuporteArquivos;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\SuporteCategoria;
use Medilaudo\Models\SuporteArquivo;
use Medilaudo\Libs\Thumbs;

class SuporteArquivosController extends Controller
{
    protected $pathImagens = 'suporte_arquivos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $categorias_id = $request->categorias_id;
      $categorias = SuporteCategoria::ordenado()->get();

      if($categorias_id)
        $registros  = SuporteArquivo::categoria($categorias_id)->ordenado()->get();
      else
        $registros  = SuporteArquivo::ordenado()->get();

      return view('painel.suporte_arquivos.index')->with(compact('registros', 'categorias', 'categorias_id'));
    }

    public function create(Request $request)
    {
      $categorias = SuporteCategoria::ordenado()->get();
      return view('painel.suporte_arquivos.create')->with(compact('categorias'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'categorias_id' => 'required|exists:suporte_categorias,id',
        'titulo' => 'required',
        'arquivo' => 'required|mimes:pdf'
    	]);

      $object = new SuporteArquivo;

      $object->categorias_id = $request->categorias_id;
      $object->titulo = $request->titulo;
      $object->descritivo = $request->descritivo;

      $arquivo = $request->arquivo;
      if($arquivo){
        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();

        $arquivo->move('assets/arquivos/', $filename);

        $object->arquivo = $filename;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.suporte_arquivos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $categorias = SuporteCategoria::ordenado()->get();
      return view('painel.suporte_arquivos.edit')->with('registro', SuporteArquivo::find($id))
                                                 ->with(compact('categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'categorias_id' => 'required|exists:suporte_categorias,id',
        'titulo' => 'required',
        'arquivo' => 'sometimes|mimes:pdf'
    	]);

      $object = SuporteArquivo::find($id);

      $object->categorias_id = $request->categorias_id;
      $object->titulo = $request->titulo;
      $object->descritivo = $request->descritivo;

      $arquivo = $request->arquivo;
      if($arquivo){
        $arquivo_antigo = $object->arquivo;

        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();

        $arquivo->move('assets/arquivos/', $filename);

        $object->arquivo = $filename;
        @unlink('assets/arquivos/'.$arquivo_antigo);
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.suporte_arquivos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = SuporteArquivo::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.suporte_arquivos.index');
    }

}
