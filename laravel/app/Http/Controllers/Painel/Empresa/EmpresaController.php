<?php

namespace Medilaudo\Http\Controllers\Painel\Empresa;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Libs\Thumbs;
use Medilaudo\Models\Empresa;
use Medilaudo\Models\EmpresaImagem;

class EmpresaController extends Controller
{
    protected $pathImagens = 'empresa/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Empresa::get();

      return view('painel.empresa.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $imagens = EmpresaImagem::ordenado()->get();
      return view('painel.empresa.edit')->with('registro', Empresa::find($id))
                                        ->with(compact('imagens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'subtitulo' => 'required'
    	]);

      $object = Empresa::find($id);

      $object->subtitulo = $request->subtitulo;
      $object->texto_esquerda = $request->texto_esquerda;
      $object->texto_direita = $request->texto_direita;

      try {

        $object->save();

        $imgs_atuais = EmpresaImagem::all();
        foreach ($imgs_atuais as $img_atual)
          $img_atual->delete();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            EmpresaImagem::create([
              'imagem' => $img,
              'ordem' => $ordem
            ]);
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.empresa.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
