<?php

namespace Medilaudo\Http\Controllers\Painel\MotivosMedicos;

use Illuminate\Http\Request;
use Medilaudo\Http\Controllers\Controller;

use Medilaudo\Models\MotivosMedicos;
use Medilaudo\Libs\Thumbs;

class MotivosMedicosController extends Controller
{
    protected $pathImagens = 'motivos_medicos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = MotivosMedicos::ordenado()->get();

      return view('painel.motivos_medicos.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.motivos_medicos.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'texto' => 'required'
    	]);

      $object = new MotivosMedicos;

      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.motivos_medicos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.motivos_medicos.edit')->with('registro', MotivosMedicos::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'texto' => 'required'
    	]);

      $object = MotivosMedicos::find($id);

      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.motivos_medicos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = MotivosMedicos::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.motivos_medicos.index');
    }

}
